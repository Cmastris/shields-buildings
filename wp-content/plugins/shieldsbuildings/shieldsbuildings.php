<?php

/**
 * @package Shileds Buildings
 */
/*
Plugin Name: Shields Buildings Plugin
Description: Shields Buildings plugin.
Version: 4.0
Author: Mihály Szűcs
*/


// PRODUCTS
function create_post_type() {

    $labels = array(
        'name'               => 'Shields Products',
        'singular_name'      => 'Product',
        'menu_name'          => 'Shields Products',
        'name_admin_bar'     => 'Product',
        'add_new'            => 'Add New',
        'add_new_item'       => 'Add New Product',
        'new_item'           => 'New Product',
        'edit_item'          => 'Edit Product',
        'view_item'          => 'View Product',
        'all_items'          => 'All Products',
        'search_items'       => 'Search Products',
        'parent_item_colon'  => 'Parent Product',
        'not_found'          => 'No Products',
        'not_found_in_trash' => 'No Product Found in Trash'
    );

    $args = array(
        'labels'              => $labels,
        'public'              => true,
        'exclude_from_search' => false,
        'publicly_queryable'  => true,
        'show_ui'             => true,
        'show_in_nav_menus'   => true,
        'show_in_menu'        => true,
        'show_in_admin_bar'   => true,
        'menu_position'       => 5,
        'menu_icon'           => 'dashicons-admin-appearance',
        'capability_type'     => 'post',
        'hierarchical'        => false,
        'supports'            => array( 'title' ),
        'has_archive'         => true,
        'rewrite'             => array( 'slug' => 'products' ),
        'query_var'           => true
    );

    register_post_type('product', $args);

}

function create_taxonomies() {

    // Add a taxonomy like categories
    $labels = array(
        'name'              => 'Types',
        'singular_name'     => 'Type',
        'search_items'      => 'Search Types',
        'all_items'         => 'All Types',
        'parent_item'       => 'Parent Type',
        'parent_item_colon' => 'Parent Type:',
        'edit_item'         => 'Edit Type',
        'update_item'       => 'Update Type',
        'add_new_item'      => 'Add New Type',
        'new_item_name'     => 'New Type Name',
        'menu_name'         => 'Types',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'type'),
    );

    register_taxonomy('product_type', array('product'), $args);
    function checktoradio(){
        echo '<script type="text/javascript">jQuery("#product_typechecklist-pop input, #product_typechecklist input, .cat-checklist input").each(function(){this.type="radio"});</script>';
    }
    add_action('admin_footer', 'checktoradio');

    add_rewrite_rule('^products/(.*)/(.*)$', 'index.php?product=$matches[2]', 'top');
    add_rewrite_rule('^products/(.*)$', 'index.php?product_type=$matches[1]', 'top');
    
    // Add a taxonomy like categories
    $labels = array(
        'name'              => 'Options',
        'singular_name'     => 'Option',
        'search_items'      => 'Search Option',
        'all_items'         => 'All Option',
        'parent_item'       => 'Parent Option',
        'parent_item_colon' => 'Parent Option:',
        'edit_item'         => 'Edit Option',
        'update_item'       => 'Update Option',
        'add_new_item'      => 'Add New Option',
        'new_item_name'     => 'New Option Name',
        'menu_name'         => 'Options',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'option' ),
    );

    register_taxonomy('product_option', array('product'), $args);
	
    // Add a taxonomy like categories per category
    $labels = array(
        'name'              => 'Options Category',
        'singular_name'     => 'Option Category',
        'search_items'      => 'Search Option Category',
        'all_items'         => 'All Option Category',
        'parent_item'       => 'Parent Option Category',
        'parent_item_colon' => 'Parent Option Category:',
        'edit_item'         => 'Edit Option Category',
        'update_item'       => 'Update Option Category',
        'add_new_item'      => 'Add New Option Category',
        'new_item_name'     => 'New Option Category Name',
        'menu_name'         => 'Options Category',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'option_category' ),
    );

    register_taxonomy('product_option_category', array('product'), $args);
        
    // Add a taxonomy like categories
    $labels = array(
        'name'              => 'Pricing Examples',
        'singular_name'     => 'Pricing Example',
        'search_items'      => 'Search Pricing Examples',
        'all_items'         => 'All Pricing Examples',
        'parent_item'       => 'Parent Pricing Example',
        'parent_item_colon' => 'Parent Pricing Example:',
        'edit_item'         => 'Edit Pricing Example',
        'update_item'       => 'Update Pricing Example',
        'add_new_item'      => 'Add New Pricing Example',
        'new_item_name'     => 'New Pricing Example Name',
        'menu_name'         => 'Pricing Examples',
    );

    $args = array(
        'hierarchical'      => true,
        'labels'            => $labels,
        'show_ui'           => true,
        'show_admin_column' => true,
        'query_var'         => true,
        'rewrite'           => array( 'slug' => 'price' ),
    );

    register_taxonomy('product_pricing_example', array('product'), $args);


            
    // Add term page
    function asdasf() {
        // this will add the custom meta field to the add new term page
        ?>
            <div class="form-field">
                <label for="term_meta[custom_term_meta]"><?php _e( 'Price', 'pippin' ); ?></label>
                <input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="">
                <p class="description"><?php _e( 'Enter a value for this field','pippin' ); ?></p>
            </div>
        <?php
    }
    add_action('product_pricing_example_add_form_fields', 'asdasf', 10, 2 );

    function asfasfasfasf($term) {
        
            // put the term ID into a variable
            $t_id = $term->term_id;
        
            // retrieve the existing value(s) for this meta field. This returns an array
            $term_meta = get_option( "taxonomy_$t_id" ); ?>
            <tr class="form-field">
            <th scope="row" valign="top"><label for="term_meta[custom_term_meta]"><?php _e( 'Price', 'pippin' ); ?></label></th>
                <td>
                    <input type="text" name="term_meta[custom_term_meta]" id="term_meta[custom_term_meta]" value="<?php echo esc_attr( $term_meta['custom_term_meta'] ) ? esc_attr( $term_meta['custom_term_meta'] ) : ''; ?>">
                    <p class="description"><?php _e( 'Enter a value for this field','pippin' ); ?></p>
                </td>
            </tr>
        <?php
    }

    add_action('product_pricing_example_edit_form_fields', 'asfasfasfasf', 10, 2 );

    function save_taxonomy_custom_meta( $term_id ) {
        if ( isset( $_POST['term_meta'] ) ) {
            $t_id = $term_id;
            $term_meta = get_option( "taxonomy_$t_id" );
            $cat_keys = array_keys( $_POST['term_meta'] );
            foreach ( $cat_keys as $key ) {
                if ( isset ( $_POST['term_meta'][$key] ) ) {
                    $term_meta[$key] = $_POST['term_meta'][$key];
                }
            }
            // Save the option array.
            update_option( "taxonomy_$t_id", $term_meta );
        }
    }

    add_action('edited_product_pricing_example', 'save_taxonomy_custom_meta', 10, 2 );  
    add_action('create_product_pricing_example', 'save_taxonomy_custom_meta', 10, 2 );

}

add_action('init', 'create_post_type');
add_action('init', 'create_taxonomies');

	
function product_columns_head($defaults) {
    $defaults['featured'] = 'Featured';
    return $defaults;
}
add_filter('manage_product_posts_columns', 'product_columns_head');

function product_columns_content($column_name, $post_ID) {
    if ($column_name == 'featured') {
    	$featured = get_post_meta($post_ID, $column_name);
    	echo isset($featured[0]) ? "Yes" : "No";
    }
}
add_filter('manage_product_posts_custom_column', 'product_columns_content', 10, 2);
	
	
	

add_filter('rwmb_meta_boxes', 'dog_meta_boxes' );
function dog_meta_boxes( $meta_boxes ) {
    $meta_boxes[] = array(
        'title'      => __( 'Add New Product', 'textdomain' ),
        'post_types' => 'product',
        'fields'     => array(
            array(
                'id'      => 'introduction',
                'name'    => __( 'Introduction', 'textdomain' ),
                'type'    => 'textarea'
            ),
            array(
                'id'      => 'introductionimage',
                'name'    => __( 'Introduction Image', 'textdomain' ),
                'type'    => 'image_advanced'
            ),
            array(
                'id'      => 'introductionvideo',
                'name'    => __( 'Introduction Video Url', 'textdomain' ),
                'type'    => 'text'
            ),
            array(
                'id'      => 'whatsgreat',
                'name'    => __( 'Whats Great', 'textdomain' ),
                'type'    => 'wysiwyg'
            ),
            array(
                'id'      => 'benefits',
                'name'    => __( 'Benefits', 'textdomain' ),
                'type'    => 'wysiwyg'
            ),
            array(
                'id'      => 'highlights',
                'name'    => __( 'Highlights', 'textdomain' ),
                'type'    => 'wysiwyg'
            ),
            array(
                'id'      => 'timber',
                'name'    => __( 'Quality & Luxury', 'textdomain' ),
                'type'    => 'textarea'
            ),
            array(
                'id'      => 'timberimage',
                'name'    => __( 'Quality & Luxury Image', 'textdomain' ),
                'type'    => 'image_advanced'
            ),
            array(
                'id'      => 'optiondescription',
                'name'    => __( 'Product Options Description', 'textdomain' ),
                'type'    => 'textarea'
            ),
            array(
                'id'      => 'featured',
                'name'    => __( 'Featured', 'textdomain' ),
                'type'    => 'checkbox'
            ),
            array(
                'id'      => 'sliderimages',
                'name'    => __( 'Slider Images', 'textdomain' ),
                'type'    => 'image_advanced'
            ),
            array(
                'id'      => 'getaquote',
                'name'    => __( 'Get a quote', 'textdomain' ),
                'type'    => 'textarea'
            ),
            array(
                'id'      => 'pricingexamples',
                'name'    => __( 'Pricing Examples', 'textdomain' ),
                'type'    => 'textarea'
            )
        ),
    );
    return $meta_boxes;
}

// exit if file is called directly
if (!defined('ABSPATH')) {
    exit;
}

define('SBPPATH',   plugin_dir_path(__FILE__));

include_once(SBPPATH . '/include/class.optionsmenu.php');
include_once(SBPPATH . '/include/class.brochures.php');
include_once(SBPPATH . '/include/class.homeslider.php');
include_once(SBPPATH . '/include/class.casestudies.php');
include_once(SBPPATH . '/include/class.testimonials.php');
include_once(SBPPATH . '/include/class.highlights.php');
include_once(SBPPATH . '/include/class.supports.php');
include_once(SBPPATH . '/include/class.teammembers.php');
include_once(SBPPATH . '/include/class.news.php');

new OptionsMenu();
new Brochures();
new HomeSlider();
new CaseStudies();
new Testimonials();
new Highlights();
new Supports();
new TeamMembers();
new News();
