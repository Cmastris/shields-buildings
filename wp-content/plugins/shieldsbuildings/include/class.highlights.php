<?php

class Highlights {
    
    function __construct() {

        $this->createPostType();
        $this->setupMetaBoxes();

    }

    function createPostType() {
        
        function create_post_type_highlight() {
            
                $labels = array(
                    'name'               => 'Shields Highlights',
                    'singular_name'      => 'Highlight',
                    'menu_name'          => 'Shields Highlights',
                    'name_admin_bar'     => 'Highlight',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New Highlight',
                    'new_item'           => 'New Highlight',
                    'edit_item'          => 'Edit Highlight',
                    'view_item'          => 'View Highlight',
                    'all_items'          => 'All Highlight',
                    'search_items'       => 'Search Highlight',
                    'parent_item_colon'  => 'Parent Highlight',
                    'not_found'          => 'No Highlights',
                    'not_found_in_trash' => 'No Highlights Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array(''),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'highlights' ),
                    'query_var'           => true
                );
            
                register_post_type('highlight', $args);
            
            }
        
        add_action('init', 'create_post_type_highlight');

    }

    function setupMetaBoxes() {
        
        add_filter('rwmb_meta_boxes', 'highlights_metaboxes' );
        function highlights_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New Highlight', 'textdomain' ),
                'post_types' => 'highlight',
                'fields'     => array(
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    ),
                    array(
                        'id'      => 'lead',
                        'name'    => __( 'Lead', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'title',
                        'name'    => __( 'Title', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'textarea'
                    )
                ),
            );
            return $meta_boxes;
        }

    }

}