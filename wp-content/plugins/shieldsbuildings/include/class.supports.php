<?php

class Supports {
    
    function __construct() {

        $this->createPostType();
		$this->createTypeTaxonomy();
        $this->setupMetaBoxes();
		add_action('init', array($this, 'addRewriteRule'));

    }
	
	function addRewriteRule() {
		
		add_rewrite_rule('^support/(.*)$', 'index.php?support=$matches[1]', 'top');
		
	}

    function createPostType() {
        
        function create_post_type_support() {
            
                $labels = array(
                    'name'               => 'Supports',
                    'singular_name'      => 'Case Study',
                    'menu_name'          => 'Shields Supports',
                    'name_admin_bar'     => 'Support',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New Support',
                    'new_item'           => 'New Support',
                    'edit_item'          => 'Edit Support',
                    'view_item'          => 'View Support',
                    'all_items'          => 'All Supports',
                    'search_items'       => 'Search Supports',
                    'parent_item_colon'  => 'Parent Supports',
                    'not_found'          => 'No Supports',
                    'not_found_in_trash' => 'No Supports Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array('title'),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'support' ),
                    'query_var'           => true
                );
            
                register_post_type('support', $args);
            
            }
        
        add_action('init', 'create_post_type_support');

    }
	
	function createTypeTaxonomy() {
		
		function createTaxonomy() {
		
			$labels = array(
		        'name'              => 'Types',
		        'singular_name'     => 'Type',
		        'search_items'      => 'Search Types',
		        'all_items'         => 'All Types',
		        'parent_item'       => 'Parent Type',
		        'parent_item_colon' => 'Parent Type:',
		        'edit_item'         => 'Edit Type',
		        'update_item'       => 'Update Type',
		        'add_new_item'      => 'Add New Type',
		        'new_item_name'     => 'New Type Name',
		        'menu_name'         => 'Types',
		    );
		
		    $args = array(
		        'hierarchical'      => true,
		        'labels'            => $labels,
		        'show_ui'           => true,
		        'show_admin_column' => true,
		        'query_var'         => true,
		        'rewrite'           => array( 'slug' => 'support_type'),
		    );
			
		    register_taxonomy('support_type', array('support'), $args);
		
		}
		
		add_action('init', 'createTaxonomy');
			
	}

    function setupMetaBoxes() {
        
        add_filter('rwmb_meta_boxes', 'supports_metaboxes' );
        function supports_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New Case Study', 'textdomain' ),
                'post_types' => 'support',
                'fields'     => array(
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'introduction',
                        'name'    => __( 'Introduction', 'textdomain' ),
                        'type'    => 'wysiwyg'
                    ),
                    array(
                        'id'      => 'highlights',
                        'name'    => __( 'Highlights', 'textdomain' ),
                        'type'    => 'post',
                        'post_type' => 'highlight',
                        'field_type' => 'checkbox_list'
                    ),
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    ),
		            array(
		                'id'      => 'sliderimages',
		                'name'    => __( 'Slider Images', 'textdomain' ),
		                'type'    => 'image_advanced'
		            )
                ),
            );
            return $meta_boxes;
        }

    }

}