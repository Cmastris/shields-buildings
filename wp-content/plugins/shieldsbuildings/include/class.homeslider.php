<?php

class HomeSlider {

    function __construct() {
        $this->createPostType();
        $this->setupMetaBoxes();
    }

    function createPostType() {

        function create_post_type_home_slider() {
            
                $labels = array(
                    'name'               => 'Shields Home Slider',
                    'singular_name'      => 'Home Slider',
                    'menu_name'          => 'Shields Home Slider',
                    'name_admin_bar'     => 'Home Slider',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New Slider Element',
                    'new_item'           => 'New Slider Element',
                    'edit_item'          => 'Edit Slider Element',
                    'view_item'          => 'View Slider Element',
                    'all_items'          => 'All Slider Elements',
                    'search_items'       => 'Search Slider Elements',
                    'parent_item_colon'  => 'Parent Slider Elements',
                    'not_found'          => 'No Slider Elements',
                    'not_found_in_trash' => 'No Slider Elements Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array('title'),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'home-slider' ),
                    'query_var'           => true
                );
            
                register_post_type('homeslider', $args);
            
            }
        
        add_action('init', 'create_post_type_home_slider');

    }

    function setupMetaBoxes() {

        add_filter('rwmb_meta_boxes', 'home_slider_metaboxes' );
        function home_slider_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New Home Slider Element', 'textdomain' ),
                'post_types' => 'homeslider',
                'fields'     => array(
                    array(
                        'id'      => 'heading',
                        'name'    => __( 'Heading', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'url',
                        'name'    => __( 'Button Url', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'buttontext',
                        'name'    => __( 'Button Text', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    )
                ),
            );
            return $meta_boxes;
        }

    }

}
