<?php

class CaseStudies {
    
    function __construct() {

        $this->createPostType();
        $this->setupMetaBoxes();
		add_action('init', array($this, 'addRewriteRule'));

    }
	
	function addRewriteRule() {
		
		add_rewrite_rule('^case-studies/(.*)$', 'index.php?casestudy=$matches[1]', 'top');
		
	}

    function createPostType() {
        
        function create_post_type_casestudy() {
            
                $labels = array(
                    'name'               => 'Case Studies',
                    'singular_name'      => 'Case Study',
                    'menu_name'          => 'Case Studies',
                    'name_admin_bar'     => 'Case Study',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New Case Study',
                    'new_item'           => 'New Case Study',
                    'edit_item'          => 'Edit Case Study',
                    'view_item'          => 'View Case Study',
                    'all_items'          => 'All Case Studies',
                    'search_items'       => 'Search Case Studies',
                    'parent_item_colon'  => 'Parent Case Studies',
                    'not_found'          => 'No Case Studies',
                    'not_found_in_trash' => 'No Case Studies Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array('title'),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'case-studies' ),
                    'query_var'           => true
                );
            
                register_post_type('casestudy', $args);
            
            }
        
        add_action('init', 'create_post_type_casestudy');

    }

    function setupMetaBoxes() {
        
        add_filter('rwmb_meta_boxes', 'casestudys_metaboxes' );
        function casestudys_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New Case Study', 'textdomain' ),
                'post_types' => 'casestudy',
                'fields'     => array(
                    array(
                        'id'      => 'subtitle',
                        'name'    => __( 'Subtitle', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    ),
		            array(
		                'id'      => 'sliderimages',
		                'name'    => __( 'Slider Images', 'textdomain' ),
		                'type'    => 'image_advanced'
		            ),
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'workingtogether',
                        'name'    => __( 'Working Together', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'overcomingobstacles',
                        'name'    => __( 'Overcoming Obstacles', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'whatweachieved',
                        'name'    => __( 'What Was Achieved', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'highlights',
                        'name'    => __( 'Highlights', 'textdomain' ),
                        'type'    => 'post',
                        'post_type' => 'highlight',
                        'field_type' => 'checkbox_list'
                    ),
                    array(
                        'id'      => 'product',
                        'name'    => __( 'Product', 'textdomain' ),
                        'type'    => 'post',
                        'post_type' => 'product',
                        'field_type' => 'select'
                    ),
		            array(
		                'id'      => 'featured',
		                'name'    => __( 'Featured', 'textdomain' ),
		                'type'    => 'checkbox'
		            ),
                    array(
                        'id'      => 'productbutton',
                        'name'    => __( 'Product Url', 'textdomain' ),
                        'type'    => 'post',
                        'post_type' => 'product',
                        'field_type' => 'select'
                    ),
                    array(
                        'id'      => 'video',
                        'name'    => __( 'Video testimonial', 'textdomain' ),
                        'type'    => 'text'
                    )
                ),
            );
            return $meta_boxes;
        }

    }

}