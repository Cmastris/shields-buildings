<?php

class Brochures {
    
    function __construct() {

        $this->createPostType();
        $this->setupMetaBoxes();

    }

    function createPostType() {
        
        function create_post_type_brochure() {
            
                $labels = array(
                    'name'               => 'Shields Brochures',
                    'singular_name'      => 'Brochure',
                    'menu_name'          => 'Shields Brochures',
                    'name_admin_bar'     => 'Brochure',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New Brochure',
                    'new_item'           => 'New Brochure',
                    'edit_item'          => 'Edit Brochure',
                    'view_item'          => 'View Brochure',
                    'all_items'          => 'All Brochures',
                    'search_items'       => 'Search Brochures',
                    'parent_item_colon'  => 'Parent Brochures',
                    'not_found'          => 'No Brochures',
                    'not_found_in_trash' => 'No Brochures Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array(''),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'brochures' ),
                    'query_var'           => true
                );
            
                register_post_type('brochure', $args);
            
            }
        
        add_action('init', 'create_post_type_brochure');

    }

    function setupMetaBoxes() {
        
        add_filter('rwmb_meta_boxes', 'brochures_metaboxes' );
        function brochures_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New Brochure', 'textdomain' ),
                'post_types' => 'brochure',
                'fields'     => array(
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    ),
                    array(
                        'id'      => 'title',
                        'name'    => __( 'Title', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
                    array(
                        'id'      => 'brochure',
                        'name'    => __( 'Brochure', 'textdomain' ),
                        'type'    => 'file',
                        'max_file_uploads' => 1
                    )
                ),
            );
            return $meta_boxes;
        }

    }

}