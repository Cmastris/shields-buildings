<?php

class TeamMembers {
    
    function __construct() {

        $this->createPostType();
        $this->setupMetaBoxes();

    }

    function createPostType() {
        
        function create_post_type_teammember() {
            
                $labels = array(
                    'name'               => 'Shields TeamMembers',
                    'singular_name'      => 'TeamMember',
                    'menu_name'          => 'Shields TeamMembers',
                    'name_admin_bar'     => 'TeamMember',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New TeamMember',
                    'new_item'           => 'New TeamMember',
                    'edit_item'          => 'Edit TeamMember',
                    'view_item'          => 'View TeamMember',
                    'all_items'          => 'All TeamMember',
                    'search_items'       => 'Search TeamMember',
                    'parent_item_colon'  => 'Parent TeamMember',
                    'not_found'          => 'No TeamMember',
                    'not_found_in_trash' => 'No TeamMember Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array('title'),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'teammember' ),
                    'query_var'           => true
                );
            
                register_post_type('teammember', $args);
            
            }
        
        add_action('init', 'create_post_type_teammember');

    }

    function setupMetaBoxes() {
        
        add_filter('rwmb_meta_boxes', 'teammembers_metaboxes' );
        function teammembers_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New TeamMember', 'textdomain' ),
                'post_types' => 'teammember',
                'fields'     => array(
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    ),
                    array(
                        'id'      => 'subtitle',
                        'name'    => __( 'Sub Title', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'wysiwyg'
                    )
                ),
            );
            return $meta_boxes;
        }

    }

}