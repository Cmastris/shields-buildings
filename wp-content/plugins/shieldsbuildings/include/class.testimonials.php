<?php

class Testimonials {
    
    function __construct() {

        $this->createPostType();
        $this->setupMetaBoxes();
		$this->modifyListColumns();
		$this->modifyColumns();

    }
	
	function modifyListColumns() {
		
		function testimonial_columns_head($defaults) {
		    $defaults['featured'] = 'Featured';
		    return $defaults;
		}
		
		add_filter('manage_testimonial_posts_columns', 'testimonial_columns_head');
		
	}
	
	function modifyColumns() {
			 
		function testimonial_columns_content($column_name, $post_ID) {
		    if ($column_name == 'featured') {
		    	$featured = get_post_meta($post_ID, $column_name);
		    	echo isset($featured[0]) ? "Yes" : "No";
		    }
		}
		
		add_filter('manage_testimonial_posts_custom_column', 'testimonial_columns_content', 10, 2);
		
	}

    function createPostType() {
        
        function create_post_type_testimonial() {
            
                $labels = array(
                    'name'               => 'Testimonials',
                    'singular_name'      => 'Testimonial',
                    'menu_name'          => 'Testimonials',
                    'name_admin_bar'     => 'Testimonial',
                    'add_new'            => 'Add New',
                    'add_new_item'       => 'Add New Testimonial',
                    'new_item'           => 'New Testimonial',
                    'edit_item'          => 'Edit Testimonial',
                    'view_item'          => 'View Testimonial',
                    'all_items'          => 'All Testimonials',
                    'search_items'       => 'Search Testimonials',
                    'parent_item_colon'  => 'Parent Testimonials',
                    'not_found'          => 'No Testimonials',
                    'not_found_in_trash' => 'No Testimonials Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array(''),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'testimonials' ),
                    'query_var'           => true
                );
            
                register_post_type('testimonial', $args);
            
            }
        
        add_action('init', 'create_post_type_testimonial');

    }

    function setupMetaBoxes() {
        
        add_filter('rwmb_meta_boxes', 'testimonials_metaboxes' );
        function testimonials_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add New Testimonial', 'textdomain' ),
                'post_types' => 'testimonial',
                'fields'     => array(
                    array(
                        'id'      => 'lead',
                        'name'    => __( 'Lead', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'title',
                        'name'    => __( 'Title', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'description',
                        'name'    => __( 'Description', 'textdomain' ),
                        'type'    => 'textarea'
                    ),
		            array(
		                'id'      => 'featured',
		                'name'    => __( 'Featured', 'textdomain' ),
		                'type'    => 'checkbox'
		            ),
                    array(
                        'id'      => 'casestudy',
                        'name'    => __( 'Case Study', 'textdomain' ),
                        'type'    => 'post',
                        'post_type' => 'casestudy',
                        'field_type' => 'select'
                    ),
                    array(
					    'name'       => 'Connected Product Category',
					    'id'         => 'testomonialproductcategory',
					    'type'       => 'taxonomy',
					    'taxonomy'   => 'product_type',
					    'field_type' => 'select'
					)
                ),
            );
            return $meta_boxes;
        }

    }

}