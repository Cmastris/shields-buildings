<?php

class News {

    function __construct() {
        $this->createPostType();
		$this->createTypeTaxonomy();
		$this->setupMetaBoxes();
		
		add_filter("get_archives_link", "archiveUrlChange");
		function archiveUrlChange($d) {
			return str_replace(home_url("/"), home_url("/")."news/", $d);
		}
		
		add_filter("author_link", "author_link_url_change");
		function author_link_url_change($link) {
			return str_replace(home_url("/"), home_url("/")."news/", $link);
		}
		
		add_filter('getarchives_where', 'news_archive_where',10,2);
		function news_archive_where($where,$args) {
		    $post_type  = isset($args['post_type'])  ? $args['post_type']  : 'post';  
		    $where = "WHERE post_type = '$post_type' AND post_status = 'publish'";
		    return $where;  
		}
		
		
		add_action('init', 'news_archive_rewrite');
		function news_archive_rewrite(){
		   add_rewrite_rule('^news/([0-9]{4})/([0-9]{2})/?','index.php?post_type=news&year=$matches[1]&monthnum=$matches[2]', 'top');
		   add_rewrite_rule('^news/([0-9]{4})/?','index.php?post_type=news&year=$matches[1]', 'top');
		   add_rewrite_rule('^news/author/?','index.php?post_type=news&author_name=$matches[1]', 'top');
		   add_rewrite_rule('^news/category/?/page/?/','index.php?post_type=news&category=$matches[1]&paged=$matches[2]', 'top');
		   add_rewrite_rule('^news/category/?','index.php?post_type=news&category=$matches[1]', 'top');
		   add_rewrite_rule('^news/search/?','index.php?post_type=news', 'top');
		   add_rewrite_rule('^news/?$','index.php?post_type=news&news=$matches[1]', 'top');
		}

		
    }

    function createPostType() {

        function create_post_type_news() {
            
                $labels = array(
                    'name'               => 'Shields News',
                    'singular_name'      => 'News',
                    'menu_name'          => 'Shields News',
                    'name_admin_bar'     => 'News',
                    'add_new'            => 'Add News',
                    'add_new_item'       => 'Add News',
                    'new_item'           => 'New News',
                    'edit_item'          => 'Edit News',
                    'view_item'          => 'View News',
                    'all_items'          => 'All News',
                    'search_items'       => 'Search News',
                    'parent_item_colon'  => 'Parent News',
                    'not_found'          => 'No News',
                    'not_found_in_trash' => 'No News Found in Trash'
                );
            
                $args = array(
                    'labels'              => $labels,
                    'public'              => true,
                    'exclude_from_search' => false,
                    'publicly_queryable'  => true,
                    'show_ui'             => true,
                    'show_in_nav_menus'   => true,
                    'show_in_menu'        => true,
                    'show_in_admin_bar'   => true,
                    'menu_position'       => 5,
                    'menu_icon'           => 'dashicons-admin-appearance',
                    'capability_type'     => 'post',
                    'hierarchical'        => false,
                    'supports'            => array('title', 'editor', 'author'),
                    'has_archive'         => true,
                    'rewrite'             => array( 'slug' => 'news' ),
                    'query_var'           => true
                );
            
                register_post_type('news', $args);
            
            }
        
        add_action('init', 'create_post_type_news');

    }
	
	function createTypeTaxonomy() {
		
		function createNewsTaxonomy() {
		
			$labels = array(
		        'name'              => 'Types',
		        'singular_name'     => 'Type',
		        'search_items'      => 'Search Types',
		        'all_items'         => 'All Types',
		        'parent_item'       => 'Parent Type',
		        'parent_item_colon' => 'Parent Type:',
		        'edit_item'         => 'Edit Type',
		        'update_item'       => 'Update Type',
		        'add_new_item'      => 'Add New Type',
		        'new_item_name'     => 'New Type Name',
		        'menu_name'         => 'Types',
		    );
		
		    $args = array(
		        'hierarchical'      => true,
		        'labels'            => $labels,
		        'show_ui'           => true,
		        'show_admin_column' => true,
		        'query_var'         => true,
		        'rewrite'           => array( 'slug' => 'news_type'),
		    );
			
		    register_taxonomy('news_type', array('news'), $args);
		
		}
		
		add_action('init', 'createNewsTaxonomy');
			
	}

    function setupMetaBoxes() {

        add_filter('rwmb_meta_boxes', 'news_metaboxes' );
        function news_metaboxes( $meta_boxes ) {
            $meta_boxes[] = array(
                'title'      => __( 'Add News', 'textdomain' ),
                'post_types' => 'news',
                'fields'     => array(
                    array(
                        'id'      => 'heading',
                        'name'    => __( 'Heading', 'textdomain' ),
                        'type'    => 'text'
                    ),
                    array(
                        'id'      => 'image',
                        'name'    => __( 'Image', 'textdomain' ),
                        'type'    => 'image_advanced'
                    )
                ),
            );
            return $meta_boxes;
        }

    }

}
