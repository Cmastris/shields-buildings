<?php

add_action('wp_dashboard_setup', 'my_custom_dashboard_widgets');

function my_custom_dashboard_widgets() {
	wp_add_dashboard_widget('custom_help_widget', 'Shields Plugin', 'custom_dashboard_help');
}

function custom_dashboard_help() {

	$a = wp_count_posts("product");
	$b = wp_count_posts("brochures");
	$c = wp_count_posts("homeslider");

	echo "<b>Published products:</b> " . $a -> publish . "<br />";
	echo "<b>Published brochures:</b> " . $b -> publish . "<br />";
	echo "<b>Published home sliders:</b> " . $c -> publish;

}

function pr($d) {
	echo "<pre>";
	print_r($d);
	echo "</pre>";
}

function metaConvert($meta) {
	$std = new StdClass();
	foreach ($meta as $key => $value) {
		$std -> $key = $value[0];
	}
	return $std;
}

function getNews() {
	
	$query = new WP_Query(array('post_type' => 'news', 'nopaging' => false, 'posts_per_page' => 4, 'orderby' => "date", 'order' => "desc"));
	$news = $query->posts;
	
	foreach ($news as $new) {
		$postTerms = get_post_meta($new -> ID);
		$new -> meta = metaConvert($postTerms);
		$new -> meta -> image = wp_get_attachment_url($new -> meta -> image);
	}
	
	return $news;
	
}

function getBrochures() {

	$query = new WP_Query( array('post_type' => 'brochure'));
	$brochures = $query -> posts;

	foreach ($brochures as $brochure) {
		$postTerms = get_post_meta($brochure -> ID);
		$brochure -> meta = metaConvert($postTerms);
		$brochure -> meta -> image = wp_get_attachment_url($brochure -> meta -> image);
	}

	return $brochures;

}

function getTestimonials($all = false) {

	$query = new WP_Query( array('post_type' => 'testimonial', 'nopaging' => false, 'posts_per_page' => $all ? 999999 : 3, 'orderby' => "rand"));
	$testimonials = $query -> posts;

	foreach ($testimonials as $testimonial) {
		$postTerms = get_post_meta($testimonial -> ID);
		$testimonial -> meta = metaConvert($postTerms);
		$testimonial -> meta -> image = wp_get_attachment_url($testimonial -> meta -> image);
		$testimonial_temp=wp_get_post_terms($testimonial -> ID, "product_type");
		$testimonial -> category = $testimonial_temp[0];
	}

	return $testimonials;

}

function getTeamMembers() {

	$query = new WP_Query( array('post_type' => 'teammember', 'nopaging' => false, 'posts_per_page' => 3, 'orderby' => "rand"));
	$teammembers = $query -> posts;

	foreach ($teammembers as $teammember) {
		$postTerms = get_post_meta($teammember -> ID);
		$teammember -> meta = metaConvert($postTerms);
		$teammember -> meta -> image = wp_get_attachment_url($teammember -> meta -> image);
		$teammember_temp=wp_get_post_terms($teammember -> ID, "product_type");
		$teammember -> category = $teammember_temp[0];
	}

	return $teammembers;

}

function getProcudtsCategoriesForArchive() {

	$categories = get_terms(array('taxonomy' => 'product_type', 'hide_empty' => false));

	foreach ($categories as $category) {

		$category -> products = new WP_Query( array('post_type' => 'product', 'tax_query' => array( array('taxonomy' => 'product_type', //Taxonomy Name
		'field' => 'id', 'terms' => $category -> term_id))));
		$category -> products = $category -> products -> posts;
		foreach ($category->products as $product) {
			$product -> meta = metaConvert(get_post_meta($product -> ID));
			$product -> meta -> introductionimage = wp_get_attachment_url($product -> meta -> introductionimage);
		}

	}

	return $categories;

}

function getNewsByName($name) {
	
	$query = get_posts(array('name' => $name, 'post_type' => 'news', 'numberposts' => 1));
	
	$news = $query[0];
	
	$news->meta = metaConvert(get_post_meta($news->ID));
	$news->meta->image = wp_get_attachment_url($news->meta->image);
	
	return $news;
	
	
}

function getProductById($id) {

	$product = get_post($id);
	
	$product->types = get_the_terms($id, "product_type");
	$product->types = $product->types[0];
	
	return $product;
	
}

function getProductByName($name) {

	$query = get_posts(array('name' => $name, 'post_type' => 'product', 'numberposts' => 1));

	$product = $query[0];

	$product -> meta = metaConvert(get_post_meta($product -> ID));
	$product -> meta -> introduction = str_replace("\r\n", "<br />", $product -> meta -> introduction);
	$product -> meta -> timberimage = wp_get_attachment_url($product -> meta -> timberimage);

	$product -> pricing = get_the_terms($product -> ID, "product_pricing_example");
	if ($product -> pricing)
		foreach ($product->pricing as $productPrice) {
			$option_temp=get_option("taxonomy_" . $productPrice -> term_taxonomy_id);
			$productPrice -> price = $option_temp["custom_term_meta"];
		}

	$product -> caseStudies = new WP_Query( array('post_type' => 'casestudy', 'meta_query' => array( array('key' => 'product', 'value' => $product -> ID, 'compare' => '=', ))));
	$product -> caseStudies = $product -> caseStudies -> posts;

	foreach ($product->caseStudies as $caseStudy) {
		$caseStudy -> meta = metaConvert(get_post_meta($caseStudy -> ID));
		$caseStudy -> meta -> image = wp_get_attachment_url($caseStudy -> meta -> image);
	}

	$product -> options = get_the_terms($product -> ID, "product_option");
	$product -> optionsImages = apply_filters('taxonomy-images-get-terms', '', array('taxonomy' => 'product_option'));

	if ($product -> options)
		foreach ($product->options as $productOption) {
			if ($product -> optionsImages)
				foreach ($product->optionsImages as $productOptionsImage) {
					if ($productOption -> term_id == $productOptionsImage -> term_id) {
						$productOption -> image = wp_get_attachment_url($productOptionsImage -> image_id);
					}
				}
		}

	$product -> types = get_the_terms($product -> ID, "product_type");

	$product->sliderImages = rwmb_meta("sliderimages", 'type=image_advanced&size=full', $product->ID);
	
	return $product;

}

function getSupportTypes() {

	$supportTypes = get_terms('support_type', array('hide_empty' => false, ));

	if ($supportTypes)
		foreach ($supportTypes as $supportType) {

			$supportType -> supports = get_posts(array("post_type" => "support", "nopaging" => true, 'tax_query' => array( array('taxonomy' => 'support_type', 'field' => 'id', 'terms' => $supportType -> term_id))));

			if ($supportType -> supports)
				foreach ($supportType->supports as $support) {
					$support -> meta = metaConvert(get_post_meta($support -> ID));
					$support -> meta -> image = wp_get_attachment_url($support -> meta -> image);
				}

		}

	return $supportTypes;

}

function getSupportByName($name) {

	$query = get_posts(array('name' => $name, 'post_type' => 'support', 'numberposts' => 1));

	$support = $query[0];

	$support -> meta = metaConvert(get_post_meta($support -> ID));
	$support -> meta -> highlights = get_post_meta($support -> ID, "highlights");

	$highlights = get_posts(array('post_type' => 'highlight', 'post__in' => $support -> meta -> highlights));

	if ($highlights)
		foreach ($highlights as $highlight) {
			$highlight -> meta = metaConvert(get_post_meta($highlight -> ID));
			$highlight -> meta -> image = wp_get_attachment_url($highlight -> meta -> image);
		}

	$support -> meta -> highlights = $highlights;

	return $support;

}

function get_authors_ids_by_post_type($type = null, $args = '') {
	if ($type === null)
		return;
	global $wpdb;
	$defaults = array('orderby' => 'name', 'order' => 'ASC', 'number' => '', 'optioncount' => false, 'exclude_admin' => true, 'show_fullname' => false, 'hide_empty' => true, 'feed' => '', 'feed_image' => '', 'feed_type' => '', 'echo' => true, 'style' => 'list', 'html' => true);

	$args = wp_parse_args($args, $defaults);
	extract($args, EXTR_SKIP);

	$return = '';

	$query_args = wp_array_slice_assoc($args, array('orderby', 'order', 'number'));
	$query_args['fields'] = 'ids';
	$authors = get_users($query_args);
	$author_count = array();
	foreach ((array) $wpdb->get_results("SELECT DISTINCT post_author, COUNT(ID) AS count FROM $wpdb->posts WHERE post_type = '$type' AND " . get_private_posts_cap_sql( $type ) . " GROUP BY post_author") as $row)
		$author_count[$row -> post_author] = $row -> count;
	foreach ($authors as $author_id) {
		$author = get_userdata($author_id);

		if ($exclude_admin && 'admin' == $author -> display_name)
			continue;

		$posts = isset($author_count[$author -> ID]) ? $author_count[$author -> ID] : 0;

		if (!$posts && $hide_empty)
			continue;

		$link = '';

		if ($show_fullname && $author -> first_name && $author -> last_name)
			$name = "$author->first_name $author->last_name";
		else
			$name = $author -> display_name;

		if (!$html) {
			$return .= $name . ', ';

			continue;
			// No need to go further to process HTML.
		}

		if ('list' == $style) {
			$return .= '<li>';
		}

		$link = '<a href="' . get_author_posts_url($author -> ID, $author -> user_nicename) . '" title="' . esc_attr(sprintf(__("Posts by %s"), $author -> display_name)) . '">' . $name . '</a>';

		if (!empty($feed_image) || !empty($feed)) {
			$link .= ' ';
			if (empty($feed_image)) {
				$link .= '(';
			}

			$link .= '<a href="' . get_author_feed_link($author -> ID) . '"';
			$alt = $title = '';
			if (!empty($feed)) {
				$title = ' title="' . esc_attr($feed) . '"';
				$alt = ' alt="' . esc_attr($feed) . '"';
				$name = $feed;
				$link .= $title;
			}

			$link .= '>';

			if (!empty($feed_image))
				$link .= '<img src="' . esc_url($feed_image) . '" style="border: none;"' . $alt . $title . ' />';
			else
				$link .= $name;

			$link .= '</a>';

			if (empty($feed_image))
				$link .= ')';
		}

		if ($optioncount)
			$link .= ' (' . $posts . ')';

		$return .= $link;

		$return .= ('list' == $style) ? '</li>' : ', ';

	}

	$return = rtrim($return, ', ');

	if (!$echo)
		return $return;

	echo $return;
}

add_filter( 'tiny_mce_before_init', 'mce_custom_fonts' );
function mce_custom_fonts( $init ) {
    $theme_advanced_fonts = "Andale Mono=andale mono,times;" .
                            "Arial=arial,helvetica,sans-serif;" .
                            "Arial Black=arial black,avant garde;" .
                            "Book Antiqua=book antiqua,palatino;" .
                            "Comic Sans MS=comic sans ms,sans-serif;" .
                            "Courier New=courier new,courier;" .
                            "Georgia=georgia,palatino;" .
                            "Helvetica=helvetica;" .
                            "Impact=impact,chicago;" .
                            "FiraSans=Fira Sans;" . /* This is my custom font */
                            "Symbol=symbol;" .
                            "Tahoma=tahoma,arial,helvetica,sans-serif;" .
                            "Terminal=terminal,monaco;" .
                            "Times New Roman=times new roman,times;" .
                            "Trebuchet MS=trebuchet ms,geneva;" .
                            "Verdana=verdana,geneva;" .
                            "Webdings=webdings;" .
                            "Wingdings=wingdings,zapf dingbats";
    $init['font_formats'] = $theme_advanced_fonts;
    return $init;
}
