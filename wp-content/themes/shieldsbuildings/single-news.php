<?php get_header(); ?>

<?php $request_temp=array_reverse(explode("/", $wp->request));$news = getNewsByName($request_temp[0]); ?>

<div id="cms-page-image" class="sr" style="background-image: url('<?=$news->meta->image?>')"></div>

<div id="cms-page-conatiner" class="sr">
	<div id="cms-page-conatiner-left" class="cms">
		
		<div class="lead"><?=$news->meta->heading?></div>
		
		<h1><?=$news->post_title?></h1>
		
		<div id="news-spacing-title"></div>
		
		<?=$news->post_content?>
		
	</div>
	<div id="cms-page-conatiner-right">
		<?php get_template_part('template-part-download-and-planning') ?>
	</div>
</div>

<?php get_footer(); ?>