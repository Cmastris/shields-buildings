<?php get_header(); ?>

<script src="https://maps.googleapis.com/maps/api/js?v=3.exp&key=<?=do_shortcode('[cwd ref="maps_api_key"]')?>&sensor=false"></script>
 
<script>
	
	function initialize() {
	    var myLatlng = new google.maps.LatLng(50.786843,-3.894919);
	    var mapOptions = {
	        zoom: 15,
	        center: myLatlng,
	        mapTypeId: google.maps.MapTypeId.ROADMAP
	    }
	    var map = new google.maps.Map(document.getElementById('contact-right'), mapOptions);
	
	     //=====Initialise Default Marker    
	    var marker = new google.maps.Marker({
	        position: myLatlng,
	        map: map,
	        title: 'marker'
	     //=====You can even customize the icons here
	    });
	
	     //=====Initialise InfoWindow
	    var infowindow = new google.maps.InfoWindow({
	      content: ""
	  });
	
	   //=====Eventlistener for InfoWindow
	  google.maps.event.addListener(marker, 'click', function() {
	    infowindow.open(map,marker);
	  });
	}
	
	google.maps.event.addDomListener(window, 'load', initialize);
	
</script>

<div id="contact" class="sr">
 	<div id="contact-left">
 		<div id="contact-left-block-left">
	 		<div class="lead">Location</div>
	 		<h2 id="contact-left-title">Visit Us</h2>
	 		<div id="contact-left-description"><?=do_shortcode('[cwd ref="contact_page_visit_us_description"]')?></div>
	 		<div id="contact-left-info"><?=str_replace("\r\n", "<br />", do_shortcode('[cwd ref="contact_page_visit_us_info"]'))?></div>
 		</div>
 		<div id="contact-left-block-right">
	 		<div class="lead">Directions</div>
	 		<h2 id="contact-left-title">How to find us</h2>
	 		<div id="contact-left-description"><?=str_replace("\r\n", "<br />", do_shortcode('[cwd ref="contact_how_to_find_us_description"]'))?></div>
 		</div>
 	</div>
 	<div id="contact-right"></div>
</div>

<form id="contact-form" class="sr">
	<div id="contact-blocks">
		<div class="lead">Email</div>
		<div id="contact-blocks-container">
			<div class="contact-block contact-block-title">
				<div id="contact-block-title">Send us a message</div>
				<div id="contact-block-description"><?=do_shortcode('[cwd ref="contact_send_us_a_message_description"]')?></div>
			</div>
			<div class="contact-block contact-block-center">
				<select name="title">
					<option value="0">Select Title</option>
					<option value="Mr">Mr</option>
					<option value="Mrs">Mrs</option>
					<option value="Miss">Miss</option>
					<option value="Ms">Ms</option>
					<option value="Dr">Dr</option>
    			</select>
				<input type="text" name="your_full_name" class="contact-form-row" placeholder="Your full name" />
				<input type="text" name="your_email" class="contact-form-row" placeholder="Your email" />
				<input type="text" name="your_phone" class="contact-form-row" placeholder="Your telephone no." />
			</div>
			<div class="contact-block">
				<textarea placeholder="Message" name="message" class="contact-form-row"></textarea>
				<div id="contact-block-submit">
					<div id="contact-block-submit-left">
						<input type="checkbox" id="newsletter" name="newsletter" /><label id="label-for-newsletter" for="newsletter"></label> Sign up for our newsletter updates
					</div>
					<div id="contact-block-submit-right">
						<input type="submit" class="button btn_160 orange" value="Send Enquiry" />
					</div>
					<div id="contact-form-error"></div>
				</div>
			</div>
		</div>
	</div>
</form>

<?php get_template_part('template-part-separator') ?>

<div id="contact-members-heading" class="sr">
	<div class="lead">The Team</div>
	<div id="contact-members-heading-title">Our staff members</div>
</div>

<?php $teammembers = getTeamMembers(); ?>

<?php $i = 0; ?>
<?php foreach ($teammembers as $teammember) { ?>
	<div class="contact-page-team-memeber-box <?=($i%2==1?"right":"left")?>">
		<div class="contact-page-team-memeber-box-image" style="background-image: url('<?=$teammember->meta->image?>')"></div>
		<div class="contact-page-team-memeber-box-data">
			<div class="lead"><?=$teammember->meta->subtitle?></div>
			<div class="contact-page-team-memeber-box-title"><?=$teammember->post_title?></div>
			<div class="contact-page-team-memeber-box-description"><?=$teammember->meta->description?></div>
		</div>
	</div>
	<?php get_template_part('template-part-separator') ?>
	<?php $i++; ?>
<?php } ?>


<?php

	/*
	
	$items = [];
	$i = 0;
	foreach ($teammembers as $teammember) {
		$item = new stdClass();
		$item->image = $teammember->meta->image;
		$item->lead = $teammember->meta->subtitle;
		$item->title = $teammember->post_title;
		$item->description = $teammember->meta->description;
		$items[] = $item;
		$i++;
	}

	set_query_var( 'items', $items );
	set_query_var( 'autoBoxHeight', true );
	get_template_part('template-part-black-boxes');
	
	 * */

?>

<?php get_template_part('template-part-news-and-events') ?>

<div id="contact-footer-extend"></div>

<script type="text/javascript">
	
	$(document).ready(function () {
		
		$("#label-for-newsletter").click(function () {
			$(this).toggleClass("checked");
		})
		
	})
	
</script>

<?php get_footer(); ?>