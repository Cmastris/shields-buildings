<?php get_header(); ?>

<?php

    $testimonials = getTestimonials(true);

    $query = new WP_Query(array('post_type'=> 'casestudy', 'nopaging' => true));
    $casestudies = $query->posts;
	
    foreach ($casestudies as $casestudy) {
        $postTerms = get_post_meta($casestudy->ID);
        $casestudy->meta = metaConvert($postTerms);
        $casestudy->meta->image = wp_get_attachment_url($casestudy->meta->image);
        $casestudy_temp=wp_get_post_terms($casestudy->ID, "product_type");
        $casestudy->category = $casestudy_temp[0];
		
		$casestudy->product = getProductById($casestudy->meta->product);
    }

    $categories = get_terms(array('taxonomy' => 'product_type', 'hide_empty' => false));

    foreach ($categories as $category) {
        $category->caseStudies = array();
        foreach ($casestudies as $casestudy) {
            if ($casestudy->product->types->term_id == $category->term_id) {
                $category->caseStudies[] = $casestudy;
            }
        }
    }

?>

<div id="case-studies">
    <div id="case-studies-lead" class="lead">LIVE PROJECTS</div>
    <h1 id="case-studies-title">Case Studies</h1>
    <div id="case-studies-description"><?=do_shortcode('[cwd ref="case_studies_main_page_description"]')?></div>
    <div id="case-studies-contaier">
        <?php foreach ($categories as $category) { ?>
            <div class="case-studies-category-container">
                <div class="lead">CASE STUDIES</div>
                <h2 class="case-studies-category-title"><?=$category->name?></h2>
                <div class="case-studies-slider">
                    <?php $i = 1; ?>
                    <?php foreach ($category->caseStudies as $casestudy) { ?>
                        <div class="case-study">
                        	<a href="<?=get_site_url()?>/case-studies/<?=$casestudy->post_name?>/">
	                            <div class="case-study-image-container" style="background-image: url('<?=$casestudy->meta->image?>');"></div>
	                            <div class="case-study-lead lead"><?=$casestudy->meta->subtitle?></div>
	                            <h1 class="case-study-title"><?=$casestudy->post_title?></h1>
	                            <div class="case-study-description"><?=$casestudy->meta->description?></div>
                            </a>
                        </div>
                        <?php $i++; ?>
                    <?php } ?>
                </div>
            </div>
        <?php } ?>
    </div>
</div>

<a name="testimonials"></a>
<div id="case-studies-testimonials">
    <div id="case-studies-testimonials-lead" class="lead">CUSTOMER FEEDBACK</div>
    <h2 id="case-studies-testimonials-title">Testimonials</h2>
    <div id="case-studies-testimonials-description"><?=do_shortcode('[cwd ref="testimonials_description"]')?></div>
    <div id="case-studies-testimonials-container">
        <?php foreach ($testimonials as $testimonial) { ?>
            <div class="case-studies-testimonial">
                <div class="lead"><?=$testimonial->meta->lead?></div>
                <div class="case-studies-testimonial-description"><?=$testimonial->meta->description?></div>
                <div class="case-studies-testimonial-title"><?=$testimonial->meta->title?></div>
            </div>
            <?php $i++; ?>
        <?php } ?>
    </div>
</div>

<?php get_footer(); ?>