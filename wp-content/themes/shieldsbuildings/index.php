<?php

$current_url = "https://".$_SERVER['HTTP_HOST'].$_SERVER['REQUEST_URI'];
$current_url = str_replace(get_site_url(), "", $current_url);
$current_url = str_replace("/", "", $current_url);

if ($current_url != "") {
	get_header();
	
    $cmsPageData = get_page_by_path($current_url);
	
	?>
	<div id="cms-page-image" style="background-image: url('<?=get_template_directory_uri()?>/images/cms.jpg')"></div>
	
	<div id="cms-page-conatiner">
		<div id="cms-page-conatiner-left" class="cms">
			
			<div class="lead"><?=$cmsPageData->post_title?></div>
			
			<?=$cmsPageData->post_content?>
			
		</div>
		<div id="cms-page-conatiner-right">
			<?php get_template_part('template-part-download-and-planning') ?>
		</div>
	</div>
	<?php
	
	get_footer();
	return;
}


get_header();

/* HOMESLIDES */
function getHomeSliderTerms($id) {
    $homeSliderTerms = get_post_meta($id);
    $std = new stdClass();
    foreach ($homeSliderTerms as $key => $value) {
        $std->$key = $value[0];
    }
    return $std;
}

function getHomeSliders() {
    $query = new WP_Query(array(
        'post_type'=> 'homeslider'
    ));

    $homeSliders = $query->posts;
    foreach ($homeSliders as $homeSlider) {
        $homeSlider->meta = getHomeSliderTerms($homeSlider->ID);
        $homeSlider->meta->image = wp_get_attachment_url($homeSlider->meta->image);
    }

    return $homeSliders;
}



$homeSliders = getHomeSliders();

/* GET FEATURED PRODUCTS */
function getProductTerms($id) {
    $homeSliderTerms = get_post_meta($id);
    $std = new stdClass();
    foreach ($homeSliderTerms as $key => $value) {
        $std->$key = $value[0];
    }
    return $std;
}

remove_all_filters('posts_orderby');
$query = new WP_Query(array(
    'post_type' => 'product',
    'nopaging' => false,
    'posts_per_page' => 3,
    'meta_key' => 'featured',
    'meta_value' => 1,
    'orderby' => "rand"
));
$featuredProducts = $query->posts;
foreach ($featuredProducts as $featuredProduct) {
	$featuredProduct->terms = getProductTerms($featuredProduct->ID);
	$featuredProduct->terms->introductionimage = wp_get_attachment_url($featuredProduct->terms->introductionimage);
	$featuredProduct_temp=get_the_terms($featuredProduct->ID, "product_type");
    $featuredProduct->productType = $featuredProduct_temp[0];
}

/* GET TESTIMOIALS */
function getTestimonialTerms($id) {
    $homeSliderTerms = get_post_meta($id);
    $std = new stdClass();
    foreach ($homeSliderTerms as $key => $value) {
        $std->$key = $value[0];
    }
    return $std;
}

remove_all_filters('posts_orderby');
$query = new WP_Query(array(
    'post_type' => 'testimonial',
    'nopaging' => false,
    'posts_per_page' => 1,
    'meta_key' => 'featured',
    'meta_value' => 1,
    'orderby' => "rand"
));
$featuredTestimonials = $query->posts;
foreach ($featuredTestimonials as $featuredTestimonial) {
	$featuredTestimonial->terms = getTestimonialTerms($featuredTestimonial->ID);
    $featuredTestimonial_temp=get_the_terms($featuredTestimonial->ID, "product_type");
	$featuredTestimonial->productType =  $featuredTestimonial_temp[0];
}

remove_all_filters('posts_orderby');
$query = new WP_Query(array(
    'post_type' => 'casestudy',
    'nopaging' => false,
    'posts_per_page' => 1,
    'meta_key' => 'featured',
    'meta_value' => 1,
    'orderby' => "rand"
));
$featuredCaseStudies = $query->posts;
foreach ($featuredCaseStudies as $featuredCaseStudy) {
	$featuredCaseStudy->terms = getTestimonialTerms($featuredCaseStudy->ID);
    $featuredCaseStudy_temp=get_the_terms($featuredCaseStudy->ID, "product_type");
	$featuredCaseStudy->productType = $featuredCaseStudy_temp[0];
	$featuredCaseStudy->terms->image = wp_get_attachment_url($featuredCaseStudy->terms->image);
}

?>

<div id="home-slider" class="home-slider sr">
    <?php foreach ($homeSliders as $homeSlider) { ?>
        <div class="home-slider-element">
            <div class="home-slider-image" style="background-image: url('<?=$homeSlider->meta->image?>')"></div>
            <div class="home-text-container">
            	<div class="slick-arrow slick-prev"></div>
                <div class="home-text-lead lead"><?=$homeSlider->meta->heading?></div>
                <h2 class="home-text-title"><?=$homeSlider->post_title?></h2>
                <div class="home-text-description"><?=$homeSlider->meta->description?></div>
                <div class="home-text-button-container">
                    <a href="<?=$homeSlider->meta->url?>" class="button btn_200"><?=$homeSlider->meta->buttontext?></a>

                </div>
                <div class="slick-arrow slick-next"></div>
            </div>
        </div>
    <?php } ?>
</div>

<div id="home-introduction" class="sr">
    <div id="home-introduction-left">
        <iframe width="560" height="315" src="<?=do_shortcode('[cwd ref="home_page_introduction_video"]')?>" frameborder="0" allowfullscreen></iframe>
    </div>
    <div id="home-introduction-right">
        <div id="home-introduction-lead" class="lead">Introduction</div>
        <h2 id="home-introduction-title"><?=do_shortcode('[cwd ref="home_page_introduction_title"]')?></h2>
        <div id="home-introduction-description"><?=do_shortcode('[cwd ref="home_page_introduction_description"]')?></div>
        <a href="<?=do_shortcode('[cwd ref="home_page_introduction_learn_more_button_url"]')?>" class="button btn_120">Learn More</a>
    </div>
</div>

<?php 

	$productCategories = apply_filters('taxonomy-images-get-terms', '', array('taxonomy' => 'product_type'));
	
	$productCategoriesTmp = array();
	foreach ($productCategories as $productCategory) {
		$productCategory->meta = get_term_meta($productCategory->term_id);
		$productCategory->image = wp_get_attachment_url($productCategory->image_id);
		if ($productCategory->term_id != 18) {
			$productCategoriesTmp[] = $productCategory;
		}
	}
	$productCategories = $productCategoriesTmp;

	$items = array();
	foreach ($productCategories as $productCategory) {
		$item = new stdClass();
		$item->image = $productCategory->image;
		$item->lead = "FEATURED PRODUCT";
		$item->title = $productCategory->name;
		$item->description = $productCategory->description;
		$item->url = get_site_url()."/products/".$productCategory->slug;
		$items[] = $item;
	}

	set_query_var( 'items', $items );
	get_template_part('template-part-black-boxes');

?>

<?php get_template_part('template-part-support-guidance') ?>

<?php get_template_part('template-part-download-and-planning') ?>

<div id="featured-items" class="sr">
    <div id="featured-items-top"></div>
    <div id="featured-items-bottom"></div>
    <div id="featured-items-left" style="background-image: url('<?=$featuredCaseStudies[0]->terms->image?>')"></div>
    <div id="featured-items-right">
        <div class="featured-item">
            <div class="lead">FEATURED TESTIMONIAL</div>
            <div class="featured-item-title">What our customers say</div>
            <div class="featured-item-description featured-item-description-testimonial">“<?php if ($featuredTestimonials) echo $featuredTestimonials[0]->description ?>”</div>
            <a href="<?=get_site_url()?>/case-studies/#testimonials" class="button btn_180 orange">More Testimonials</a>
        </div>
        <div class="featured-item">
            <div class="lead">FEATURED CASE STUDY</div>
            <div class="featured-item-title"><?php if ($featuredCaseStudies) echo $featuredCaseStudies[0]->post_title ?></div>
            <div class="featured-item-description"><?php if ($featuredCaseStudies) echo $featuredCaseStudies[0]->terms->workingtogether ?></div>
            <a href="<?=get_site_url()?>/case-studies/" class="button btn_180 orange">More Case Studies</a>
        </div>
    </div>
</div>

<?php get_template_part('template-part-news-and-events') ?>


<div id="newsletter" class="sr">
	<div class="lead">Keep Me posted</div>
	<div id="newsletter-title">Sign up to our Newsletter, we will keep you posted with the latest information.</div>
	<div id="newsletter-description">We will keep you posted with the latest news on Events, Open days, New Buildings & more.</div>
	<form id="newsletter-form" class="yellow-scratch-background">
		<input type="text" class="newsletter-form-row" name="your_name" placeholder="Your Name" />
		<input type="text" class="newsletter-form-row" name="your_email" placeholder="Your Email" />
		<input type="submit" class="button btn_160" value="Sign Up" />
	</form>
	<div id="newsletter-error"></div>
</div>

<?php get_footer(); ?>