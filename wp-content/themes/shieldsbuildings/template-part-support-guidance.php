<div id="products-footer" class="sr">
    <div id="products-footer-logo"></div>
    <div class="lead">SUPPORT</div>
    <div id="products-footer-title"><?=do_shortcode('[cwd ref="guidance_title"]')?></div>
    <div id="products-footer-description"><?=do_shortcode('[cwd ref="guidance_description"]')?></div>
    <div id="products-footer-button-container">
        <a href="<?=do_shortcode('[cwd ref="guidance_button_url"]')?>" class="button btn_160"><?=do_shortcode('[cwd ref="guidance_button_text"]')?></a>
    </div>
</div>