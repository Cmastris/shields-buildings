<div id="download" class="yellow-scratch-background">
    <div id="download-lead">DOWNLOAD A</div>
    <div id="download-title">
        <div id="download-title-left">7</div>
        <div id="download-title-right"><span>POINT GUIDE</span> FOR YOUR PERFECT TIMBER BUILDING</div>
    </div>
    <form action="" id="download-form" class="seven-point-guide-download-form">
        <input type="text" class="download-form-input" placeholder="Your name *" name="your_name" id="seven-pointg-guide-download-form-name" />
        <input type="text" class="download-form-input" placeholder="Your email *" name="your_email" id="seven-pointg-guide-download-form-email" />
        <input type="submit" class="button btn_160 gray" value="Download" />
        <div id="seven-point-guide-download-form-error"></div>
    </form>
</div>