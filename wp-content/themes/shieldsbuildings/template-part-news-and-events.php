<div id="news-container" class="sr">
    <div class="lead">VISIT OUR SHOWROOM AND DISPLAY BUILDINGS</div>
    <div id="news-title">News & Events</div>
    <div id="news-description"><?=do_shortcode('[cwd ref="news_and_events_description"]')?></div>
    <div id="news-button-container">
        <a href="<?=get_site_url()?>/news/" class="button">Find Out More</a>
    </div>
    <?php $news = getNews(); ?>
    <div id="news-elements-container">
    	<?php foreach ($news as $n) { ?>
	        <div class="news-element">
	            <div class="news-image-container" style="background-image: url('<?=$n->meta->image?>')"></div>
	            <div class="lead"><?=$n->meta->heading?></div>
	            <div class="news-title"><?=$n->post_title?></div>
	            <div class="news-element-button-container">
	                <a href="<?=get_site_url()?>/news/<?=$n->post_name?>/" class="button btn_92">Details</a>
	            </div>
	        </div>
        <?php } ?>
    </div>
</div>