<?php get_header(); ?>

<?php $categories = get_terms(array('taxonomy' => 'news_type', 'hide_empty' => false)); ?>

<?php

$newsArchivesArgs = array(
	'post_type' 	  => 'news',
	'type'            => 'monthly',
	'limit'           => '',
	'format'          => 'custom',
	'before'          => '',
	'after'           => '',
	'show_post_count' => false,
	'echo'            => 1,
	'order'           => 'DESC'
);

$uri = explode("/", $wp->request);

$paged = 1;
if (isset($uri[1]) && isset($uri[2]) && $uri[1] == "page") {
	$paged = $uri[2];
}
if (isset($uri[3]) && isset($uri[4]) && $uri[3] == "page") {
	$paged = $uri[4];
}
if (isset($uri[2]) && isset($uri[3]) && $uri[2] == "page") {
	$paged = $uri[3];
}

//echo $paged;

$args = array(
	'post_type' => 'news',
	'posts_per_page' => 10,
	'paged' => $paged
);

if (isset($uri[1]) && $uri[1] == "search" && isset($_GET["phrase"])) {
	$args["s"] = $_GET["phrase"];
}

$uri = explode("/", $wp->request);
if (isset($uri[1]) && $uri[1] == "author") {
	$user = get_user_by("slug", $uri[2]);
	$userId = $user->data->ID;
	$args['author__in'] = array($userId);
}

if (isset($uri[1]) && $uri[1] == "category" && isset($uri[2])) {
	$args['tax_query'] = array(
		array (
			'taxonomy' => 'news_type',
			'field' => 'slug',
			'terms' => $uri[2],
		)
	);
}

$newsquery = new WP_Query( $args );
$news = $newsquery->posts;

?>

	<div id="news-archive-container">
		<div id="news-archive-left">
			<div class="lead">Whats happening</div>
			<h1 id="news-archive-title">News from Shields Buildings</h1>
			<div id="news-archive-description"><?=do_shortcode('[cwd ref="news_main_page_description"]')?></div>
			<?php foreach ($news as $n) { ?>
				<div class="news-block sr">
					<?php $paragraphs = preg_match_all('|<p>(.+?)</p>|', $n->post_content, $matches); ?>
					<?php

					$firstP = "";
					foreach ($matches[1] as $k => $v) {
						$k = str_replace("&nbsp;", "", $v);
						$k = str_replace(" ", "", $k);
						if ($k != "")  {
							$firstP = $v;
							break;
						}
					}

					?>
					<div class="news-block-title"><?=$n->post_title?></div>
					<!--<div class="news-block-info"></div>-->
					<div class="news-block-description"><?=$firstP?></div>
					<a class="button btn_140" href="<?=get_site_url()?>/news/<?=$n->post_name?>/">Read article</a>
					<?php get_template_part('template-part-separator') ?>
				</div>
			<?php } ?>
			<div id="news-pagination">
				<?php

				echo paginate_links( array(
					'base'         => str_replace( 999999999, '%#%', esc_url( get_pagenum_link( 999999999 ) ) ),
					'total'        => $newsquery->max_num_pages,
					'current'      => max(1, $paged),
					'format'       => '?paged=%#%',
					'show_all'     => false,
					'type'         => 'plain',
					'end_size'     => 2,
					'mid_size'     => 1,
					'prev_next'    => true,
					'prev_text'    => sprintf( '<i></i> %1$s', __( 'Prev', 'text-domain' ) ),
					'next_text'    => sprintf( '%1$s <i></i>', __( 'Next', 'text-domain' ) ),
					'add_args'     => false,
					'add_fragment' => '',
				) );

				?>
			</div>
		</div>
		<div id="news-archive-right">
			<div class="news-archive-right-block">
				<div class="news-archive-right-block-title">Search</div>
				<form method="get" id="news-search-form" action="<?=get_site_url()?>/news/search/">
					<input type="text" name="phrase" id="news-search-input" value="<?=(isset($_GET["phrase"])?$_GET["phrase"]:"")?>" />
					<input type="submit" id="news-search-submit" value="" />
				</form>
			</div>
			<div class="news-archive-right-block">
				<h1 class="news-archive-right-block-title">Categories</h1>
				<?php foreach ($categories as $category) { ?>
					<a class="news-archive-right-block-url" href="<?=get_site_url()?>/news/category/<?=$category->slug?>/"><?=$category->name?></a>
				<?php } ?>
			</div>
			<div class="news-archive-right-block">
				<h1 class="news-archive-right-block-title">Authors</h1>
				<?php get_authors_ids_by_post_type("news", array("style" => "none")) ?>
			</div>
			<div class="news-archive-right-block">
				<h1 class="news-archive-right-block-title">Archive</h1>
				<?php wp_get_archives($newsArchivesArgs) ?>
			</div>
		</div>
	</div>

<?php get_footer(); ?>