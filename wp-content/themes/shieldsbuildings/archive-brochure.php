<?php get_header(); ?>

<?php $brochures = getBrochures(); ?>


<div id="brochures">

    <div id="brochures-lead" class="lead">FIND OUT MORE</div>
    <h1 id="brochures-title">Brochures</h1>
    <div id="brochures-description"><?=do_shortcode('[cwd ref="brochures_main_page_description"]')?></div>
    <div id="brochures-container">
        <?php $i = 1; ?>
        <?php foreach ($brochures as $brochure) { ?>
            <div class="brochure" data-id="<?=$brochure->ID?>">
                <div class="brochure-image">
                    <div class="brochure-image-image" style="background: url('<?=$brochure->meta->image?>')"></div>
                    <div class="brochure-image-cover"></div>
                </div>
                <div class="brochure-lead lead">Brochure <?=$i?></div>
                <div class="brochure-title"><?=$brochure->meta->title?></div>
                <div class="brochure-description"><?=$brochure->meta->description?></div>
                <div class="brochure-checkbox-container">
                	<div class="brochure-checkbox-box">
                		<div class="brochure-checkbox-element"></div>
                		<div class="brochure-checkbox-text">Select</div>
                	</div>
                </div>
            </div>
            <?php $i++; ?>
        <?php } ?>
    </div>

    <div id="brochures-download-area">
        <div class="lead">DOWNLOAD A BROCHURE</div>
        <div id="brochures-download-area-title">Brochure & Price List</div>
        <div id="brochures-download-area-description">Download a PDF of your chosen brochure by entering your details below.</div>
        <div id="brochures-download-area-inputs">
            <input type="text" id="brochures-page-download-your-name-input" placeholder="Your name *" />
            <input type="text" id="brochures-page-download-your-email-input" placeholder="Your email *" />
        </div>
        <div id="brochures-download-area-error"></div>
        <a href="#" id="brochures-page-download-button" class="button btn_160 orange">Download Now</a>
    </div>

</div>

<?php get_template_part('template-part-support-guidance') ?>

<?php get_footer(); ?>