<?php get_header(); ?>

<?php
	$request_temp=array_reverse(explode("/", $wp->request));
	$query = get_posts(array(
        'name'        => $request_temp[0],
        'post_type'   => 'casestudy',
        'numberposts' => 1
    ));
	
	$casestudy = $query[0];
	$casestudyMeta = metaConvert(get_post_meta($casestudy->ID));
	$casestudyMeta->sliderimages = rwmb_meta("sliderimages", 'type=image_advanced&size=full', $casestudy->ID);
	$casestudyMeta->highlights = get_post_meta($casestudy->ID, "highlights");
	$highlights = get_posts(array(
		'post_type' => 'highlight',
		'post__in' => $casestudyMeta->highlights
	));
	
	foreach ($highlights as $highlight) {
		$highlight->meta = metaConvert(get_post_meta($highlight->ID));
		$highlight->meta->image = wp_get_attachment_url($highlight->meta->image);
	}
	
	$testimonials = getTestimonials(true);
	$t = null;
	foreach ($testimonials as $testimonial) {
		if (isset($testimonial->meta->casestudy) && $testimonial->meta->casestudy == $casestudy->ID) {
			$t = $testimonial;
		}
	}
	$testimonial = $t;
	
?>

<div id="case-study-slider" class="sr">
	<?php foreach ($casestudyMeta->sliderimages as $k => $v) { ?>
		<div class="case-study-slider-element" style="background-image: url('<?=$v["url"]?>')"></div>
	<?php } ?>
</div>

<div id="case-study-detail">
 	<div id="case-study-detail-left">
 		<div class="lead">Case Study</div>
 		<h1 id="case-study-title"><?=$casestudy->post_title?></h1>
 		<div id="case-study-description">
 			<div class="case-study-subtitle">Working Together</div> 
 			<div class="case-study-subdescription"><?=$casestudyMeta->workingtogether?></div> 
 			<div class="case-study-subtitle">Overcoming Obstacles</div> 
 			<div class="case-study-subdescription"><?=$casestudyMeta->overcomingobstacles?></div> 
 			<div class="case-study-subtitle">What Was Achieved</div> 
 			<div class="case-study-subdescription"><?=$casestudyMeta->whatweachieved?></div> 
 			<?php if (isset($casestudyMeta->productbutton)) { ?>
 				<?php $product = getProductById($casestudyMeta->productbutton); ?>
 				<a class="case-study-product-button button green" href="<?=get_site_url()?>/products/<?=$product->types->slug?>/<?=$product->post_name?>/">View Product</a>
 			<?php } ?>
 		</div>
 	</div>
 	<div id="case-study-detail-right">
 		<div id="case-study-detail-right-description">"<?=$testimonial->meta->description?>"</div>
 		<div id="case-study-detail-right-title"><?=$testimonial->meta->title?></div>
 	</div>
</div>

<div id="case-study-highlights-heading" class="sr">
	<div class="lead">Case Study Highlights</div>
	<div id="case-study-highlights-heading-title">Case Study Highlights</div>
</div>


<?php 

	$items = array();
	$i = 0;
	foreach ($highlights as $highlight) {
		$item = new stdClass();
		$item->image = $highlight->meta->image;
		$item->lead = $highlight->meta->lead;
		$item->title = $highlight->meta->title;
		$item->description = $highlight->meta->description;
		$items[] = $item;
		$i++;
	}

	set_query_var( 'items', $items );
	get_template_part('template-part-black-boxes');

?>

<?php if ($casestudyMeta->video != "") { ?>
	<div id="case-study-video-container">
		<div class="lead">ON CAMERA</div>
		<div id="case-study-video-title">Video Testimonial</div>
		<iframe width="1140" height="560" src="<?=$casestudyMeta->video?>" frameborder="0" allowfullscreen></iframe>
	</div>
<?php } ?>

<?php get_template_part('template-part-separator') ?>

<?php get_template_part('template-part-download-and-planning') ?>

<?php get_footer(); ?>