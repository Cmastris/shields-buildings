<?php get_header(); ?>

<?php

	$brochures = getBrochures();

    $category = get_queried_object();
	
	$categoryImages = apply_filters('taxonomy-images-get-terms', '', array('taxonomy' => 'product_type'));
	
	$category->image = null;
	foreach ($categoryImages as $categoryImage) {
		if ($category->term_id == $categoryImage->term_id) {
			$category->image = wp_get_attachment_url($categoryImage->image_id);
		}
	}
	
    $term_id = $category->term_id;

    $query = new WP_Query(array(
        'post_type'=> 'product',
        'tax_query' => array(
            array(
                'taxonomy'=>'product_type',
                'field'=>'id',
                'terms'=> $term_id
            )
        )
    ));

    $products = $query->posts;
	
	$productsIds = array();
	foreach ($products as $p) {
		$productsIds[] = $p->ID;
	}
	
	$options = get_terms("product_option_category", array('hide_empty' => false));
	foreach ($options as $option) {
		$option->products = get_posts(array(
		    'post_type' => 'product',
		    'post__in' => $productsIds,
		    'tax_query' => array(
		        array(
			        'taxonomy' => 'product_option_category',
			        'field' => 'id',
			        'terms' => $option->term_id
				)
		    ))
		);
	}
	
		
		$testimonialsArray = array();
		$testimonials = getTestimonials(true);
		foreach ($testimonials as $k => $t) {
			if (isset($t->category) && !empty($t->category) && isset($t->category->term_id)) {
				if ($t->category->term_id == $category->term_id) {
					$testimonialsArray[] = $testimonials[$k];
				}
			}
		}

?>

<div id="products-header" class="sr" <?php if ($category->image != null) { ?>style="background-image: url('<?=$category->image?>') !important;" <?php } ?>>
    <div id="products-header-text-container-outer">
        <div id="products-header-text-container" class="height-not-fixed">
            <div id="products-header-text-lead" class="lead">Our range of</div>
            <h1 id="products-header-text-title"><?=$category->name?></h1>
            <div id="products-header-text-content"><?=$category->description?></div>
        </div>
    </div>
</div>

<div id="category-products-container" class="sr">
    <?php $i = 0; ?>
    <?php foreach ($products as $product) { ?>
        <?php $productTerms = get_post_meta($product->ID); ?>
        <div class="product <?=($i % 2 == 0 ? "odd" : "even")?>">
            <a href="<?=get_site_url()?>/products/<?=$category->slug?>/<?=$product->post_name?>/"><div class="product-left" style="background-image: url('<?=wp_get_attachment_url($productTerms['introductionimage'][0])?>')"></div></a>
            <div class="product-right">
                <div class="lead"><?=$category->name?></div>
                <h2 class="product-title"><?=$product->post_title?></h2>
                <div class="product-description"><?=$productTerms['introduction'][0]?></div>
                <a href="<?=get_site_url()?>/products/<?=$category->slug?>/<?=$product->post_name?>/" class="button btn_140">View Product</a>
            </div>
        </div>
        <?php if ($i != count($products) - 1) get_template_part('template-part-separator'); ?>
        <?php $i++; ?>
    <?php } ?>
</div>

<?php if ($category->term_id != 18) { ?>
<div id="product-options" class="sr">
 	<div id="product-options-left">
 		<div class="lead">product options</div>
 		<div id="product-options-title">Which is right for me?</div>
 		<div id="product-options-description"><?=do_shortcode('[cwd ref="which_is_right_for_me_'.$category->slug.'"]')?></div>
 		<a href="<?=get_site_url()?>/products/" class="button">Explore our products</a>
 	</div>
 	<div id="product-options-right">
 		<table>
 			<thead>
 				<tr>
	 				<td class="first">Option</td>
	 				<?php foreach ($products as $product) { ?>
	 					<td><?=$product->post_title?></td>
	 				<?php } ?>
 				</tr>
 			</thead>
 			<tbody>
 				<?php foreach ($options as $option) { ?>
					<?php if (count($option->products) > 0) { ?>
	 					<tr>
	 						<td class="first"><?=$option->name?></td>
			 				<?php foreach ($products as $product) { ?>
			 					<?php
			 						$has = false;
			 						foreach ($option->products as $optionproduct) {
			 							if ($optionproduct->ID == $product->ID) {
			 								$has = true;
			 							}
			 						}
			 						if ($has) {
			 							echo "<td><span class='product-option-has'></span></td>";
			 						} else {
			 							echo "<td><span class='product-option-has not'></span></td>";
			 						}
			 					?>
			 				<?php } ?>
	 					</tr>
 					<?php } ?>
 				<?php } ?>
 			</tbody>
 		</table>
 	</div>
</div>
<?php } ?>

<?php

$testimonialText = "";
if (!empty($testimonialsArray)) {
	shuffle($testimonialsArray);
	$testimonialText = $testimonialsArray[0]->meta->description;
}

?>
<div id="product-taxonomy-boxes-container">
	<div class="product-taxonomy-box product-taxonomy-box-green">
		<div class="lead">FEATURED TESTIMONIAL</div>
		<div class="product-taxonomy-box-left-title">What our customers say</div>
		<div class="product-taxonomy-box-description"><?=$testimonialText?></div>
		<div class="product-taxonomy-box-center">
			<a href="#" class="button btn_160 orange">Read More</a>
		</div>
	</div>
	<div class="product-taxonomy-box yellow-scratch-background">
		<div class="lead black">Download a copy of our</div>
		<div class="product-taxonomy-box-right-title">Brochure and price list</div>
		<form id="product_taxonomy_page_brochure_download">
			<input type="text" class="brochure-download-product-taxonomy-form-row" name="your_name" placeholder="Your name *" />
			<input type="text" class="brochure-download-product-taxonomy-form-row" name="your_email" placeholder="Your email *" />
			<select name="brochure">
				<option value="0">Select brochure</option>
				<?php foreach ($brochures as $brochure) { ?>
					<option value="<?=$brochure->ID?>"><?=$brochure->post_title?></option>
				<?php } ?>
			</select>
			<div class="product-taxonomy-box-center">
				<input type="submit" value="Download" class="button btn_160 gray" />
			</div>
			<div id="brochure-download-product-taxonomy-page-error"></div>
		</form>
	</div>
</div>

<?php get_template_part('template-part-support-guidance') ?>

<?php get_footer(); ?>