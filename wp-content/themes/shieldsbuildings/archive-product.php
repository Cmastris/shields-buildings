<?php get_header(); ?>

<?php $categories = getProcudtsCategoriesForArchive(); ?>

<div id="products-header" class="sr">
    <div id="products-header-text-container-outer">
        <div id="products-header-text-container">
            <h1 id="products-header-text-lead" class="lead">CUSTOM TIMBER BUILDINGS</h1>
            <h2 id="products-header-text-title">Our Products</h2>
            <div id="products-header-text-subtitle"><?=do_shortcode('[cwd ref="products_main_page_title"]')?></div>
            <div id="products-header-text-content"><?=do_shortcode('[cwd ref="products_main_page_description"]')?></div>
        </div>
    </div>
</div>

<div id="products-arrow" class="sr"></div>

<div id="product-categories-container">
    <?php foreach ($categories as $category) { ?>
        <div class="product-category sr">
            <div class="product-category-lead lead">OUR RANGE OF</div>
            <a href="<?=get_site_url()?>/products/<?=$category->slug?>/" class="product-category-title"><h2 class="product-category-title"><?=$category->name?></h2></a>
            <div class="product-category-description"><?=$category->description?></div>
            <div class="products-container">
                <?php foreach ($category->products as $product) { ?>
                    <div class="product <?=($product->ID==174?"wide":"")?>">
                        <a href="<?=get_site_url()?>/products/<?=$category->slug?>/<?=$product->post_name?>/">
                        	<div class="product-image" style="background-image: url('<?=$product->meta->introductionimage?>')"></div>
                        </a>
                        <div class="product-lead lead"><?=$category->name?></div>
                        <h2 class="product-title"><?=$product->post_title?></h2>
                        <div class="product-description"><?=$product->meta->introduction?></div>
                        <a href="<?=get_site_url()?>/products/<?=$category->slug?>/<?=$product->post_name?>/" class="button btn_140">View Product</a>
                        <div class="prduct-orangle"></div>
                    </div>
                <?php } ?>
            </div>
            <?php get_template_part('template-part-separator') ?>
        </div>
    <?php } ?>
</div>

<?php get_template_part('template-part-support-guidance') ?>

<?php get_footer(); ?>