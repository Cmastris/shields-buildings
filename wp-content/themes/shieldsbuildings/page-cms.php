<?php get_header(); ?>

<?php $cmsPageData = get_page_by_path('cms'); ?>

<div id="cms-page-image" class="sr" style="background-image: url('<?=get_template_directory_uri()?>/images/cms.jpg')"></div>

<div id="cms-page-conatiner" class="sr">
	<div id="cms-page-conatiner-left" class="cms">
		
		<div class="lead">Lorem ipsum</div>
		
		<?=$cmsPageData->post_content?>
		
	</div>
	<div id="cms-page-conatiner-right">
		<?php get_template_part('template-part-download-and-planning') ?>
	</div>
</div>

<?php get_footer(); ?>