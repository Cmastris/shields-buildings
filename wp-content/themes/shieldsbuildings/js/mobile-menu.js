$(document).ready(function() {
    $('#mobile_menu').click(function() {
        if ($('#header-logo-and-menu-container #header-menu-container').hasClass('active')) {
            $('#header-logo-and-menu-container #header-menu-container').removeClass('active');
        } else {
            $('#header-menu-container.active').removeClass('active');
            $('#header-logo-and-menu-container #header-menu-container').addClass('active');
        }
    });

    $(".path").click(function() {
        $(".submenu-container").toggleClass("active inactive");
        $(".path").toggleClass("opened closed");
    });
}); 