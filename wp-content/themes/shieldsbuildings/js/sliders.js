
$(document).ready(function () {
    
    var autoPlaySpeed = 6000;

    if ($(".home-slider").length > 0) {
        $(".home-slider").slick({
            appendArrows: $(".home-slider-element .home-text-container", this),
            prevArrow: "<div class='slick-arrow'></div>",
            nextArrow: "<div class='slick-arrow slick-next'></div>",
            dots: true,
            arrows: false,
            autoplay: true,
            autoplaySpeed: autoPlaySpeed
        });
        $(".slick-prev").click(function () {
            $(".home-slider").slick("slickPrev");
        });
        $(".slick-next").click(function () {
            $(".home-slider").slick("slickNext");
        });
    }
    
    if ($("#case-studies-testimonials-container").length > 0) {
        $("#case-studies-testimonials-container").slick({
            slidesToShow: 3,
            slidesToScroll: 3,
            prevArrow: "<div class='slick-arrow'></div>",
            nextArrow: "<div class='slick-arrow slick-next'></div>",
            autoplay: true,
            autoplaySpeed: autoPlaySpeed,
            responsive: [
                        {
                        breakpoint: 1280,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                            }
                        },
                        {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                            }
                        },
                        {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                            }
                        }
                ]
        });
    }
    
    if ($("#product-case-study-slider").length > 0) {
        $("#product-case-study-slider").slick({
            prevArrow: "<div class='slick-arrow'>Prev</div>",
            nextArrow: "<div class='slick-arrow slick-next'>Next</div>",
            dots: true,
            autoplaySpeed: autoPlaySpeed,
            autoplay: true
        });
    }
    
    if ($("#product-slider").length > 0) {
        $("#product-slider").slick({
            autoplay: true,
            autoplaySpeed: autoPlaySpeed,
            appendArrows: $("#product-slider-text-container"),
            prevArrow: "<div class='slick-arrow'></div>",
            nextArrow: "<div class='slick-arrow slick-next'></div>"
        });
    } 
    
    if ($(".support-section-slider").length > 0) {
        $(".support-section-slider").each(function () {
            $(this).slick({
                slidesToShow: 4,
                slidesToScroll: 4,
                prevArrow: "<div class='slick-arrow'></div>",
                nextArrow: "<div class='slick-arrow slick-next'></div>",
                responsive: [
                    {
                    breakpoint: 1280,
                    settings: {
                        slidesToShow: 3,
                        slidesToScroll: 3
                        }
                    },
                    {
                    breakpoint: 480,
                    settings: {
                        slidesToShow: 1,
                        slidesToScroll: 1
                        }
                    }
                 ]
            });
        })
    } 
    if ($("#support-slider").length > 0) {
        $("#support-slider").slick({
            autoplay: true,
            autoplaySpeed: autoPlaySpeed,
            prevArrow: "<div class='slick-arrow'></div>",
            nextArrow: "<div class='slick-arrow slick-next'></div>",
            dots: true
        });
    }
    
    if ($(".case-studies-slider").length > 0) {
        $(".case-studies-slider").each(function () {
            $(this).slick({
                infinite: true,
                slidesToShow: 3,
                slidesToScroll: 3,
                prevArrow: "<div class='slick-arrow'></div>",
                nextArrow: "<div class='slick-arrow slick-next'></div>",
                responsive: [
                        {
                        breakpoint: 1280,
                        settings: {
                            slidesToShow: 3,
                            slidesToScroll: 3
                            }
                        },
                        {
                        breakpoint: 800,
                        settings: {
                            slidesToShow: 2,
                            slidesToScroll: 2
                            }
                        },
                        {
                        breakpoint: 480,
                        settings: {
                            slidesToShow: 1,
                            slidesToScroll: 1
                            }
                        }
                     ]
            });
        })
    }
    
    if ($("#case-study-slider").length > 0) {
        $("#case-study-slider").slick({
            autoplay: true,
            autoplaySpeed: autoPlaySpeed,
            prevArrow: "<div class='slick-arrow'></div>",
            nextArrow: "<div class='slick-arrow slick-next'></div>",
            dots: true
        });
    }
    
});