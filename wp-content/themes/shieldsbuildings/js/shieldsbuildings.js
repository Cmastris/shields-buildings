
function showBrochureDownloadModal() {
    document.getElementById("brochure-download-modal").className = "active";
}

function hideBrochureDownloadModal() {
    document.getElementById("brochure-download-modal").className = "";
}

function showNewsletterModal() {
    document.getElementById("newsletter-modal").className = "active";
}

function hideNewsletterModal() {
    document.getElementById("newsletter-modal").className = "";
}

function showNewsletterModalSuccess() {
    document.getElementById("newslerre-modal-success").className = "active";
}

function hideNewsletterModalSuccess() {
    document.getElementById("newslerre-modal-success").className = "";
}

function showBrochureDownloadModalSuccess() {
    document.getElementById("brochure-download-modal-success").className = "active";
}

function hideBrochureDownloadModalSuccess() {
    document.getElementById("brochure-download-modal-success").className = "";
}

function showEnquiryModalSuccess() {
    document.getElementById("enquiry-modal-success").className = "active";
}

function hideEnquiryModalSuccess() {
    document.getElementById("enquiry-modal-success").className = "";
}

$(document).ready(function () {

    $(".scroll").click(function(event){

        event.preventDefault();

        var full_url = this.href;

        var parts = full_url.split("#");
        var trgt = parts[1];

        var target_offset = $("a[name='"+trgt+"']").offset();
        var target_top = target_offset.top;

        $('html, body').animate({scrollTop:target_top}, 500);
    });

    $("#product_taxonomy_page_brochure_download").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }


        gtag('event', 'Download', {'event_category': 'BrochureDownload', 'event_label': 'ProductCategory' });

        document.getElementById("brochure-download-product-taxonomy-page-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'brochure_download',
                data : obj
            },
            dataType: "json",
            success : function( response ) {

                document.getElementById("brochure-download-product-taxonomy-page-error").innerHTML = response.error;

                if (typeof response.fileUrl != "undefined") {

                    $(".brochure-download-product-taxonomy-form-row").val("");

                    var oReq = new XMLHttpRequest();
                    oReq.open("GET", response.fileUrl, true);
                    oReq.responseType = "blob";
                    oReq.onload = function() {
                        var file = new Blob([oReq.response], {
                            type: 'application/pdf'
                        });
                        saveAs(file, response.fileName+".pdf");
                    };
                    oReq.send();
                    console.log(oReq);

                    showBrochureDownloadModalSuccess();

                }

            }
        });

        evt.preventDefault();
        return false;
    })

    $("#brochure-download-form").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }

        gtag('event', 'Download', {'event_category': 'BrochureDownload', 'event_label': 'Header' });

        document.getElementById("brochure-download-modal-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'brochure_download',
                data : obj
            },
            dataType: "json",
            success : function( response ) {

                document.getElementById("brochure-download-modal-error").innerHTML = response.error;

                if (typeof response.fileUrl != "undefined") {

                    var oReq = new XMLHttpRequest();
                    oReq.open("GET", response.fileUrl, true);
                    oReq.responseType = "blob";
                    oReq.onload = function() {
                        var file = new Blob([oReq.response], {
                            type: 'application/pdf'
                        });
                        saveAs(file, response.fileName+".pdf");
                    };
                    oReq.send();
                    console.log(oReq);

                    hideBrochureDownloadModal();
                    showBrochureDownloadModalSuccess();

                }

            }
        });

        evt.preventDefault();
        return false;
    })

    $(".seven-point-guide-download-form").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }

        gtag('event', 'Download', {'event_category': '7PointDownload', 'event_label': 'Panel' });

        document.getElementById("seven-point-guide-download-form-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'brochure_download_7_poiunt_guide',
                data : obj
            },
            dataType: "json",
            success : function( response ) {

                document.getElementById("seven-point-guide-download-form-error").innerHTML = response.error;

                if (response.error == "") {
                    document.getElementById("seven-pointg-guide-download-form-name").value = "";
                    document.getElementById("seven-pointg-guide-download-form-email").value = "";

                    if (typeof response.fileUrl != "undefined") {

                        var oReq = new XMLHttpRequest();
                        oReq.open("GET", response.fileUrl, true);
                        oReq.responseType = "blob";
                        oReq.onload = function() {
                            var file = new Blob([oReq.response], {
                                type: 'application/pdf'
                            });
                            saveAs(file, response.fileName+".pdf");
                        };
                        oReq.send();
                        console.log(oReq);

                        hideBrochureDownloadModal();
                        showBrochureDownloadModalSuccess();

                    }

                }

            }
        });

        evt.preventDefault();
        return false;
    })

    $("#newsletter-modal-form").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }

        gtag('event', 'SignUp', {'event_category': 'NewsletterSignUp', 'event_label': 'Header' });

        document.getElementById("newsletter-modal-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'newsletter_signup',
                data : obj
            },
            dataType: "json",
            success : function( response ) {
                if (response.error == "") {
                    $(".newsletter-modal-form-row").val("");
                    hideNewsletterModal();
                    showNewsletterModalSuccess();
                }
                document.getElementById("newsletter-modal-error").innerHTML = response.error;
            }
        });

        evt.preventDefault();
        return false;
    })

    $("#newsletter-form").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }

        gtag('event', 'SignUp', {'event_category': 'NewsletterSignUp', 'event_label': 'Homepage' });


        document.getElementById("newsletter-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'newsletter_signup',
                data : obj
            },
            dataType: "json",
            success : function( response ) {
                if (response.error == "") {
                    $(".newsletter-form-row").val("");
                    showNewsletterModalSuccess();
                }
                document.getElementById("newsletter-error").innerHTML = response.error;
            }
        });

        evt.preventDefault();
        return false;
    });

    var selectedBrochures = [];
    $(".brochure").click(function () {

        $(this).siblings().each(function () {
            $(this).addClass("inactive");
            $(this).removeClass("active");
        });

        $(this).removeClass("inactive");
        $(this).addClass("active");

        selectedBrochures = [];
        selectedBrochures.push($(this).attr("data-id"));

    })

    $("#product-enquiry").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }

        gtag('event', 'Contact', {'event_category': 'Enquiry', 'event_label': 'ProductPages' });

        document.getElementById("product-enquiry-form-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'product_enquiry_form',
                data : obj
            },
            dataType: "json",
            success : function( response ) {
                if (response.error == "") {
                    $(".product-enquiry-form-row").val("");
                    hideEnquiryModalSuccess();
                    showEnquiryModalSuccess();

                }
                document.getElementById("product-enquiry-form-error").innerHTML = response.error;
            }
        });

        evt.preventDefault();
        return false;
    });

    $("#contact-form").submit(function (evt) {

        var data = $(this).serializeArray();
        var obj = {};
        for (var d in data) {
            obj[data[d].name] = data[d].value;
        }

        gtag('event', 'Contact', {'event_category': 'Enquiry', 'event_label': 'ContactPage' });

        document.getElementById("contact-form-error").innerHTML = "";

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'contact_form',
                data : obj
            },
            dataType: "json",
            success : function( response ) {
                if (response.error == "") {
                    $(".contact-form-row").val("");
                    hideEnquiryModalSuccess();
                    showEnquiryModalSuccess();
                }
                document.getElementById("contact-form-error").innerHTML = response.error;
            }
        });

        evt.preventDefault();
        return false;
    });

    $("#brochures-page-download-button").click(function (evt) {

        gtag('event', 'Download', {'event_category': 'BrochureDownload', 'event_label': 'BrochurePage' });

        $.ajax({
            url : ajaxUrl,
            type : 'post',
            data : {
                action : 'brochures_page_download',
                data : {
                    your_name: document.getElementById("brochures-page-download-your-name-input").value,
                    your_email: document.getElementById("brochures-page-download-your-email-input").value,
                    brochures: selectedBrochures
                }
            },
            dataType: "json",
            success : function( response ) {
                document.getElementById("brochures-download-area-error").innerHTML = response.error;
                if (typeof response.brochures != "undefined") {
                    for (var i = 0; i < response.brochures.length; i++) {
                        (function (d) {
                            var oReq = new XMLHttpRequest();
                            oReq.open("GET", d.fileUrl, true);
                            oReq.responseType = "blob";
                            oReq.onload = function() {
                                var file = new Blob([oReq.response], {
                                    type: 'application/pdf'
                                });
                                saveAs(file, d.post_title+".pdf");
                            };
                            oReq.send();
                            console.log(oReq);
                            hideBrochureDownloadModal();
                            showBrochureDownloadModalSuccess();
                        })(response.brochures[i]);

                    }
                }
            }
        });

        evt.preventDefault();
        return false;
    });

    function checkScroll() {
        var scrollTop = $(document).scrollTop();
        if (scrollTop >= 130) {
            $("#header-helper").addClass("float");
            $("#header").addClass("float");
        } else {
            $("#header-helper").removeClass("float");
            $("#header").removeClass("float");
        }
    }

    function checkScrollProduct() {
        var scrollTop = $(document).scrollTop();
        if (scrollTop >= 840) {
            $("#product-menu-helper").addClass("float");
            $("#product-menu").addClass("float");
        } else {
            $("#product-menu-helper").removeClass("float");
            $("#product-menu").removeClass("float");
        }
        console.log(scrollTop);
    }

    $( window ).scroll(function (evt) {
        checkScroll();
        checkScrollProduct();
    })
    checkScroll();
    checkScrollProduct();

    $(".product-menu-mobile-menu-icon").click(function () {
        $("#product-menu").toggleClass("opened");
    })

})
