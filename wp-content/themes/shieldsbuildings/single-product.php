<?php get_header(); ?>

<?php $request_temp= array_reverse(explode("/", $wp->request));
$product = getProductByName($request_temp[0]); ?>
<?php $products = new WP_Query( array("post_type" => "product") ); $products = $products->posts; ?>
<?php $category = get_the_terms($product->ID, "product_type"); ?>

<div id="product-slider-container" class="sr">
	<div id="product-slider">
		<?php foreach ($product->sliderImages as $sliderImage) { ?>
			<div class="product-slider-element" style="background-image: url('<?=$sliderImage["url"]?>')"></div>
		<?php } ?>
	</div>
	<div id="product-slider-text-container">
		<div class="lead"><?=(isset($category[0]) ? $category[0]->name : "")?></div>
		<h1 id="product-slider-text-title"><?=$product->post_title?></h1>
	</div>
</div>

<div id="product-menu-helper"></div>

<ul id="product-menu" class="sr">
    <li><a href="#introduction" class="scroll active">Introduction</a><div class="product-menu-mobile-menu-icon"></div></li>
    <li><a href="#ourtimber" class="scroll">Quality & Luxury</a></li>
    <li><a href="#productoptions" class="scroll">Product Options</a></li>
    <li><a href="#casestudies" class="scroll">Case Studies</a></li>
    <li><a href="#pricing" class="scroll">Pricing</a></li>
    <li style="border-right:none;"><a href="#support" class="scroll">Get a quote</a></li>
</ul>

<a name="introduction" class="anchor-shift"></a>
<div id="product-introduction" class="sr">
    <div id="product-introduction-left">
    	<?php
    	
    		if (isset($product->meta->introductionvideo) && $product->meta->introductionvideo != "") {
    			?> <iframe width="100%" height="315" src="<?=$product->meta->introductionvideo?>" frameborder="0" allowfullscreen></iframe> <?php
    		}
    	
    	?>
    </div>
    <div id="product-introduction-right">
        <div class="lead">Product</div>
        <div id="product-introduction-title">Introduction</div>
        <div id="product-introduction-description"><?=$product->meta->introduction?></div>
    </div>
</div>

<a name="ourtimber" class="anchor-shift"></a>
<div id="our-timber" class="sr">
	<div id="our-timber-left" style="background-image: url('<?=$product->meta->timberimage?>')"></div>
	<div id="our-timber-right">
		<div id="our-timber-right-content">
			<div class="lead" id="our-timber-lead">Bespoke</div>
			<div id="our-timber-title">Quality & Luxury</div>
			<div id="our-timber-description">
				<?=str_replace("\r\n", "<br />", $product->meta->timber)?>
			</div>
		</div>
	</div>
</div>

<a name="productoptions" class="anchor-shift"></a>
<?php if ($product->meta->optiondescription != "") { ?> 
	<?php if (isset($product->options[1])) { ?>
		<div id="product-options-detail" class="sr">
			<div id="product-options-detail-head-container">
				<div class="lead">Customise</div>
				<div id="product-options-detail-title">Product options</div>
				<div id="product-options-detail-description"><?=$product->meta->optiondescription?></div>
			</div>
			<div id="product-options-detail-images">
				<div id="product-option-detail-image-left-container">
					<div id="product-option-detail-image-left" style="background-image: url('<?=$product->options[0]->image?>')"></div>
					<div class="product-option-detail-content">
						<div class="product-option-detail-title"><?=$product->options[0]->name?></div>
						<div class="product-option-detail-description"><?=$product->options[0]->description?></div>
					</div>
				</div>
				<div id="product-option-detail-image-right-container">
					<div id="product-option-detail-image-right" style="background-image: url('<?=$product->options[1]->image?>')"></div>
					<div class="product-option-detail-content">
						<div class="product-option-detail-title"><?=$product->options[1]->name?></div>
						<div class="product-option-detail-description"><?=$product->options[1]->description?></div>
					</div>
				</div>
			</div>
		</div>
	<?php } ?>
<?php } ?>

<div id="wgbh" class="sr">
    <div id="wgbh-container">
        <div id="wgbh-container-inner">
            <div id="wgbh-lead" class="lead">BENEFITS & HIGHLIGHTS</div>
            <div class="wgbh-element">
                <div class="wgbh-element-title">What's great about this product?</div>
                <div class="wgbh-element-description"><?=$product->meta->whatsgreat?></div>
            </div>
            <div class="wgbh-element">
                <div class="wgbh-element-title">Benefits:</div>
                <div class="wgbh-element-description"><?=$product->meta->benefits?></div>
            </div>
            <div class="wgbh-element">
                <div class="wgbh-element-title">Highlights:</div>
                <div class="wgbh-element-description"><?=$product->meta->highlights?></div>
            </div>
        </div>
    </div>
</div>

<a name="pricing" class="anchor-shift"></a>
<div id="product-price" class="sr">
    <div id="product-price-left">
        <?php $i = 0; ?>
        <?php if ($product->pricing) foreach ($product->pricing as $productPrice) { ?>
            <div class="product-price-accorditon-item">
                <div class="product-price-accorditon-item-title">
                    <div class="product-price-accorditon-item-name"><?=$productPrice->name?></div>
                    <div class="product-price-accorditon-item-price">&#163;<?=$productPrice->price?></div>
                </div>
                <div class="product-price-accorditon-item-description"><?=$productPrice->description?></div>
            </div>
            <?php $i++; ?>
        <?php } ?>
        
 		<a href="<?=get_site_url()?>/products/" class="button">Explore our products</a>
    </div>
    <div id="product-price-right">
        <div class="lead">What’s the Cost</div>
        <div id="product-price-title">Pricing Examples</div>
        <div id="product-price-description"><?=$product->meta->pricingexamples?></div>
    </div>
</div>

<?php $testimonials = getTestimonials(true); ?>
<a name="casestudies" class="anchor-shift"></a>
<?php if (count($product->caseStudies) > 0) { ?>
	<div id="product-case-study-slider-container" class="sr">
		<div id="product-case-study-slider-top"></div>
		<div id="product-case-study-slider-bottom"></div>
		<div id="product-case-study-slider">
			<?php foreach ($product->caseStudies as $caseStudy) { ?>
				<?php
					
					$t = null;
					foreach ($testimonials as $testimonial) {
						if (isset($testimonial->meta->casestudy) && $testimonial->meta->casestudy == $caseStudy->ID) {
							$t = $testimonial;
						}
					}
					$testimonial = $t;
				
				?>
				<div class="product-case-study-slider-element">
					<div class="product-case-study-slider-element-left" style="background-image: url('<?=$caseStudy->meta->image?>')"></div>
					<div class="product-case-study-slider-element-right">
						<div class="lead">Related case study</div>
						<div class="product-case-study-slider-element-title"><?=$caseStudy->post_title?></div>
						<div class="product-case-study-slider-element-subtitle"><?=$caseStudy->meta->subtitle?></div>
						<div class="product-case-study-slider-element-description"><?=$caseStudy->meta->workingtogether?></div>
						<a class="button orange" href="<?=get_site_url()?>/case-studies/<?=$caseStudy->post_name?>/">View Case Study</a>
						<div class="lead">Related testimonial</div>
						<div class="product-case-study-slider-element-testimonial-title"><?=$testimonial->post_title?></div>
						<div class="product-case-study-slider-element-testimonial-description"><?=$testimonial->meta->description?></div>
					</div>
				</div>
			<?php } ?>
		</div>
	</div>
<?php } ?>

<a name="support" class="anchor-shift"></a>
<div id="product-support" class="sr">
	<div id="product-support-left">
		<div class="lead">Enquire</div>
		<div id="product-support-left-title">Request a quote</div>
		<div id="product-support-left-description"><?=$product->meta->getaquote?></div>
	</div>
	<div id="product-support-right" class="yellow-scratch-background">
		<form id="product-enquiry">
			<input type="text" name="your_name" class="product-enquiry-form-row" placeholder="Your name" value="" />
			<input type="text" name="your_email" class="product-enquiry-form-row" placeholder="Your email"  value="" />
			<input type="text" name="your_phone" class="product-enquiry-form-row" placeholder="Your telephone No."  value="" />
			<input type="text" name="your_postcode" class="product-enquiry-form-row" placeholder="Your postcode"  value="" />
			<select name="product">
				<option value="0">Select Product...</option>
				<?php foreach ($products as $product) { ?>
					<option value="<?=$product->post_title?>"><?=$product->post_title?></option>
				<?php } ?>
			</select>
			<div id="product-enquiry-form-error"></div>
			<div id="product-support-right-button-container">
				<input type="submit" class="button btn_160 gray" value="Submit Enquiry" />
			</div>
		</form>
	</div>
</div>

<script type="text/javascript">

    $(document).ready(function () {

        $(".product-price-accorditon-item-title").click(function () {
            $(this).parent().toggleClass("active");
            $(this).parent().siblings().removeClass("active");
        })
        
    })

</script>

<?php get_footer(); ?>