<div class="black-boxes-container sr">
	<?php foreach ($items as $item) { ?>
	    <div class="black-box">
	        <?php if (isset($item->url)) { ?><a href="<?=$item->url?>/"><?php } ?>
	        	<div class="black-box-image-container" style="background-image: url('<?=$item->image?>')"></div>
	        <?php if (isset($item->url)) { ?></a><?php } ?>	
	        <div class="black-box-lead lead"><?=$item->lead?></div>
	        <div class="black-box-title"><?=$item->title?></div>
	        <div class="black-box-description <?=isset($autoBoxHeight)?"height-auto":""?>"><?=$item->description?></div>
	        <?php if (isset($item->url)) { ?>
	        	<a href="<?=$item->url?>/" class="button btn_120">More Info</a>
	        <?php } ?>
	        <div class="black-box-orange-line"></div>
	    </div>
    <?php } ?>
</div>