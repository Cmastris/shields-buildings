<!DOCTYPE html>
<html>
    <head>

        <link href="https://fonts.googleapis.com/css?family=Fira+Sans:100,100i,200,200i,300,300i,400,400i,500,500i,600,600i,700,700i,800,800i,900,900i" rel="stylesheet">
        <link href="<?=get_stylesheet_uri()?>" rel="stylesheet" type="text/css" />
        <link href="<?=get_template_directory_uri()?>/responsive.css" rel="stylesheet" type="text/css" />
        <script type="text/javascript" src="<?=get_template_directory_uri()."/js/jquery-3.2.1.min.js"?>"></script>
        <script type="text/javascript" src="<?=get_template_directory_uri()."/js/slick.js"?>"></script>
        <script type="text/javascript" src="<?=get_template_directory_uri()."/js/sliders.js"?>"></script>
        <script type="text/javascript" src="<?=get_template_directory_uri()."/js/shieldsbuildings.js"?>"></script>
        <script type="text/javascript" src="<?=get_template_directory_uri()."/js/mobile-menu.js"?>"></script>
        <script type="text/javascript" src="<?=get_template_directory_uri()."/js/FileSaver.min.js"?>"></script>
        <script src="https://unpkg.com/scrollreveal/dist/scrollreveal.min.js"></script>

        <title><?= (wp_title("-", false)) ?></title>
        
        <meta name="viewport" content="width=device-width, initial-scale=1, user-scalable=no"/>
        
        <script type="text/javascript">
        	var ajaxUrl = "<?=admin_url('admin-ajax.php')?>";
        	$(document).ready(function () {
        	window.sr = ScrollReveal();
			sr.reveal('.sr');
        		
        	})
        </script>
        <!-- Global site tag (gtag.js) - Google Analytics -->
        <script async src="https://www.googletagmanager.com/gtag/js?id=UA-23171625-1"></script>
        <script>
            window.dataLayer = window.dataLayer || [];
            function gtag(){dataLayer.push(arguments);}
            gtag('js', new Date());
            gtag('config', 'UA-23171625-1');
        </script>
        <script type='text/javascript'>
            window.__lo_site_id = 85225;
            (function() {
                var wa = document.createElement('script'); wa.type = 'text/javascript'; wa.async = true;
                wa.src = 'https://d10lpsik1i8c69.cloudfront.net/w.js';
                var s = document.getElementsByTagName('script')[0]; s.parentNode.insertBefore(wa, s);
            })();
        </script>
    </head>
    <body>
    	<form id="file-download-helper"></form>
    	<div id="header-helper"></div>
    <div id="header">
        <div id="header-left">
            <a href="<?=do_shortcode('[cwd ref="facebook_page_url"]')?>" target="_blank">
                <img src="<?php bloginfo('stylesheet_directory')?>/images/header-facebook.png" /> <span>LIKE US</span>
            </a>
        </div>
        <div id="header-right">
        	<a href="javascript:showBrochureDownloadModal()" class="header-right-element brochure-icon"></a>
            <a href="javascript:showBrochureDownloadModal()" class="header-right-element brochure-link button orange">Brochure</a>
            <span class="header-right-separator"></span>
        	<a href="javascript:showBrochureDownloadModal()" class="header-right-element newsletter-icon"></a>
            <a href="javascript:showNewsletterModal()" id="header-right-newsletter-button" class="header-right-element newsletter-link">Newsletter</a>
            <span class="header-right-separator"></span>
            <a id="mobile_menu">MENU</a>
            <a href="" id="header-right-phone-number" class="header-right-element"><?=do_shortcode('[cwd ref="phone_number"]')?></a>
        </div>
        <div id="header-logo-and-menu-container">
            <a href="<?=get_site_url()?>/">
                <?
                 if ($wp->request==''){echo "<h1 id=\"header-logo\"></h1></a>";}else{echo "<div id=\"header-logo\"></div></a>";}
                ?>
            <div id="header-menu-container">
                <ul id="header-menu">
                    <li><a href="<?=get_site_url()?>/">Home</a></li>
                    <?php $pages = get_pages(array("sort_column" => "menu_order")); ?>
                    <?php foreach ($pages as $page) { ?>
                    	<?php if (!in_array($page->ID, array(129, 131, 135, 133, 137, 251))) continue; ?>
                        <li><a href="<?=get_site_url()?>/<?=$page->post_name?>/"><?=$page->post_title?></a>

                        <?php if ($page->post_name == "products") { ?>
                        	<a class="path closed"></a>
                            <div class='submenu-container inactive'>
                                <div class='submenu-content'>
                                    <?php

                                        $categories = get_terms(array('taxonomy' => 'product_type', 'hide_empty' => false));
                                        foreach ($categories as $category) {
                                            ?>
                                            <div class="submenu-content-block">
                                                <div><a href="<?=get_site_url()?>/products/<?=$category->slug?>/" class="submenu-content-block-title"><?=$category->name?></a></div>
                                                <?php

                                                    $query = new WP_Query(array(
                                                        'post_type'=> 'product',
                                                        'tax_query' => array(
                                                            array(
                                                                'taxonomy'=>'product_type', //Taxonomy Name 
                                                                'field'=>'id',
                                                                'terms'=> $category->term_id
                                                            )
                                                        )
                                                    ));
                                                    
                                                    foreach ($query->posts as $post) { ?>
                                                        <div><a href="<?=get_site_url()?>/products/<?=$category->slug?>/<?=$post->post_name?>/" class="submenu-content-block-item"><?=$post->post_title?></a></div>
                                                    <?php }

                                                ?>
                                            </div>
                                            <?php
                                        }

                                    ?>
                                </div>
                            </div>
                        <?php } ?>

                        </li>

                    <?php } ?>
                </ul>
            </div>
        </div>

    </div>