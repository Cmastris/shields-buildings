
    <div id="footer">

        <div id="footer-content">
            <div id="footer-content-container">
                <div class="footer-block">
                    <div class="lead">Visit Us</div>
                    <div class="footer-block-title">Shields Buildings</div>
                    <div class="footer-block-shields-buildings-description"><?=str_replace("\r\n", "<br />", do_shortcode('[cwd ref="footer_shields_buildings_description"]'))?></div>
                	<div class="footer-block-data">T: <a href="tel:<?=do_shortcode('[cwd ref="phone_number"]')?>"><?=do_shortcode('[cwd ref="phone_number"]')?></a></div>
                	<div class="footer-block-data">E: <a href="mailto: sales@shieldsbuildings.co.uk"><?=do_shortcode('[cwd ref="footer_email"]')?></a></div>
                </div>
                <div class="footer-block">
                    <div class="lead">Like Us</div>
                    <div class="footer-block-title">We're Social</div>
                    <div class="footer-block-we-are-social-description"><?=str_replace("\r\n", "<br />", do_shortcode('[cwd ref="footer_we_are_social_description"]'))?></div>
                	<a href="<?=do_shortcode('[cwd ref="facebook_page_url"]')?>" class="footer-facebook-button" target="_blank">
		                <img src="<?php bloginfo('stylesheet_directory')?>/images/header-facebook.png"> <span>Facebook</span>
		            </a>
					<div class="footer-block-we-are-social-tagline">Supplying the best timber buildings in the South West</div>
                </div>
                <div class="footer-block wide">
                    <div class="lead">Visit Us</div>
                    <div class="footer-block-title">Opening hours</div>
                    <div class="footer-block-opening-hours">Showroom open all day every day.</div>
                    <div class="footer-block-logo-container"><div class="footer-block-logo"></div></div>
                </div>
            </div>
        </div>
    
        <div id="copyright">
            <div id="copyright-content">
                <div id="copyright-left">
                    <span>Copyright John Shields Buildings Ltd. All rights reserved.</span>
                    <span>|</span>
                    <a href="<?=get_site_url()?>/terms-and-conditions/">Terms and Conditions of use</a>
                    <span>|</span>
                    <a href="<?=get_site_url()?>/privacy-policy/">Privacy Policy</a>
                </div>
                <div id="copyright-right">
                    <span>Designed & Developed by </span>
                    <span id="developer">Optix Solutions</span>
                </div>
            </div>
        </div>
    
    </div>
    
    <!-- BROCHURE DOWNLOAD MODAL -->
    <?php $brochures = getBrochures(); ?>
    <div id="brochure-download-modal">
    	<div id="brochure-download-modal-content">
    		<div id="brochure-download-modal-close" onclick="hideBrochureDownloadModal()"></div>
    		<div id="brochure-download-modal-content-title-container">
    			<div class="lead">Download our complete brochure</div>
    			<div id="brochure-download-modal-title">Brochure & Price List</div>
    			<div id="brochure-download-modal-description">Download a PDF file of our brochures with price list by entering your details below.</div>
    		</div>
    		<form id="brochure-download-form">
    			<div class="brochure-download-form-row">
    				<input type="text" name="your_name" class="brochure-download-form-row-input" placeholder="Your Name" />
    				<input type="text" name="your_email" class="brochure-download-form-row-input" placeholder="Your Email" />
    			</div>
    			<div class="brochure-download-form-row">
    				<select name="brochure" class="brochure-download-form-row-select">
    					<option value="0">Select brochure</option>
    					<?php foreach ($brochures as $brochure) { ?>
    						<option value="<?=$brochure->ID?>"><?=$brochure->post_title?></option>
    					<?php } ?>
    				</select>
    				<input type="submit" class="button orange" value="Download now" />
    			</div>
    		</form>
    		<div id="brochure-download-modal-error"></div>
    	</div>
    </div>
    
    <!-- NEWSLETTER MODAL -->
    <div id="newsletter-modal">
    	<div id="newsletter-modal-content">
    		<div id="newsletter-modal-close" onclick="hideNewsletterModal()"></div>
    		<div id="newsletter-modal-content-title-container">
    			<div class="lead">Keep me posted</div>
    			<div id="newsletter-modal-title"><?=do_shortcode('[cwd ref="newsletter_signup_modal_title"]')?></div>
    			<div id="newsletter-modal-description"><?=do_shortcode('[cwd ref="newsletter_signup_modal_description"]')?></div>
    		</div>
    		<form id="newsletter-modal-form">
    			<div class="newsletter-download-form-row">
    				<input type="text" name="your_name" class="newsletter-modal-form-row newsletter-download-form-row-input" placeholder="Your Name" />
    				<input type="text" name="your_email" class="newsletter-modal-form-row newsletter-download-form-row-input" placeholder="Your Email" />
    			</div>
    			<div class="newsletter-download-form-row">
    				<input type="submit" class="button btn_160" value="Sign Up" />
    			</div>
    		</form>
    		<div id="newsletter-modal-error"></div>
    	</div>
    </div>
    
    <div id="newslerre-modal-success">
    	<div id="newslerre-modal-success-content">
    		<div id="newsletter-modal-close" onclick="hideNewsletterModalSuccess()"></div>
    		<div class="lead">Thank you</div>
    		<div id="newsletter-modal-success-title">You’ve successfully signed up to our newsletter.</div>
    	</div>
    </div>
    
    <div id="brochure-download-modal-success">
    	<div id="brochure-download-modal-success-content">
    		<div id="brochure-download-modal-close" onclick="hideBrochureDownloadModalSuccess()"></div>
    		<div class="lead">Thank you</div>
    		<div id="brochure-download-modal-success-title">You’ve successfully downloaded our brochure.</div>
    	</div>
    </div>

    <div id="enquiry-modal-success">
    	<div id="enquiry-modal-success-content">
    		<div id="enquiry-modal-close" onclick="hideEnquiryModalSuccess()"></div>
    		<div class="lead">Thank you</div>
    		<div id="enquiry-modal-success-title">We will get back to you soon.</div>
    	</div>
    </div>

    </body>

</html>