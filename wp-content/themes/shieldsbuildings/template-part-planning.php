<div id="planning">
    <div id="planning-lead"><?=do_shortcode('[cwd ref="planning_permission_lead"]')?></div>
    <div id="planning-title">Planning permission</div>
    <div id="planning-description"><?=do_shortcode('[cwd ref="planning_permission_description"]')?></div>
    <a href="<?=do_shortcode('[cwd ref="planning_permission_button_url"]')?>" class="button btn_160 orange"><?=do_shortcode('[cwd ref="planning_permission_button_text"]')?></a>
</div>