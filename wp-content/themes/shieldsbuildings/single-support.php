<?php get_header(); ?>

<?php $request_temp=array_reverse(explode("/", $wp->request)); $support = getSupportByName($request_temp[0]); ?>
<?php $support->meta->sliderimages = rwmb_meta("sliderimages", 'type=image_advanced&size=full', $support->ID); ?>

<div id="support-slider" class="sr">
	<?php foreach ($support->meta->sliderimages as $k => $v) { ?>
		<div class="support-slider-element" style="background-image: url('<?=$v["url"]?>')"></div>
	<?php } ?>
</div>

<div id="support-introduction-container">
	<div class="lead">Introduction</div>
	<h1 id="support-introduction-title"><?=$support->post_title?></h1>
	<div class="cms" id="support-introduction-description">
		<?=$support->meta->introduction?>
	</div>
</div>

<div id="support-highlights-heading" class="sr">
	<div class="lead">Support highlights</div>
	<div id="support-highlights-heading-title">Support highlights</div>
</div>

<?php 

	$items = array();
	$i = 0;
	foreach ($support->meta->highlights as $highlight) {
		$item = new stdClass();
		$item->image = $highlight->meta->image;
		$item->lead = $highlight->meta->lead;
		$item->title = $highlight->meta->title;
		$item->description = $highlight->meta->description;
		$items[] = $item;
		$i++;
	}

	set_query_var( 'items', $items );
	get_template_part('template-part-black-boxes-without-anim');

?>

<?php get_template_part('template-part-separator') ?>

<?php get_template_part('template-part-download-and-planning') ?>

<?php get_footer(); ?>