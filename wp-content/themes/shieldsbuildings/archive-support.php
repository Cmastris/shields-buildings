<?php get_header(); ?>

<?php $supportTypes = getSupportTypes(); ?>

<div id="support-section-container">
	
	<h1 id="support-lead" class="sr">SUPPORT</h1>
	<div id="support-title" class="sr">Can we help you?</div>

	<?php foreach ($supportTypes as $supportType) { ?>
	<div class="support-section sr">
	    <div class="support-section-lead">Support</div>
	    <div class="support-section-title"><?=$supportType->name?></div>
	    <div class="support-section-slider">
	    	<?php foreach ($supportType->supports as $support) { ?>
		        <a href="<?=get_site_url()?>/support/<?=$support->post_name?>/" class="support-section-slider-element">
		            <div class="support-section-slider-element-image-container" style="background-image: url('<?=$support->meta->image?>')"></div>
		            <div class="support-section-slider-element-lead"><?=$supportType->name?></div>
		            <h1 class="support-section-slider-element-title"><?=$support->post_title?></h1>
		            <div class="support-section-slider-element-description"><?=$support->meta->description?></div>
		        </a>
	    	<?php } ?>
	    </div>
	</div>
	<?php } ?>
</div>

<?php get_footer(); ?>