<?php
/**
 * The base configuration for WordPress
 *
 * The wp-config.php creation script uses this file during the
 * installation. You don't have to use the web site, you can
 * copy this file to "wp-config.php" and fill in the values.
 *
 * This file contains the following configurations:
 *
 * * MySQL settings
 * * Secret keys
 * * Database table prefix
 * * ABSPATH
 *
 * @link https://codex.wordpress.org/Editing_wp-config.php
 *
 * @package WordPress
 */

// ** MySQL settings - You can get this info from your web host ** //
/** The name of the database for WordPress */
define('DB_NAME', 'shieldsbuildings');

/** MySQL database username */
define('DB_USER', 'shieldsbuildings');

/** MySQL database password */
define('DB_PASSWORD', 'dZb5FRGYHq77Nnx');

/** MySQL hostname */
define('DB_HOST', 'localhost');

/** Database Charset to use in creating database tables. */
define('DB_CHARSET', 'utf8');

/** The Database Collate type. Don't change this if in doubt. */
define('DB_COLLATE', '');

/**#@+
 * Authentication Unique Keys and Salts.
 *
 * Change these to different unique phrases!
 * You can generate these using the {@link https://api.wordpress.org/secret-key/1.1/salt/ WordPress.org secret-key service}
 * You can change these at any point in time to invalidate all existing cookies. This will force all users to have to log in again.
 *
 * @since 2.6.0
 */
define('AUTH_KEY',         'q}O= 1>Ob7^#C8hWt;P__c!#?/Lf=X9pR8Mor*7g9#/%DxeptO.>bB#y=W9:35Zz');
define('SECURE_AUTH_KEY',  'M%DbF_z``%.U95HvAEnUA}}j`Pdjrla{-6AA*Gg7a}rPM]S2ZIaC9*<_s9^Gx`OO');
define('LOGGED_IN_KEY',    'jVwjN MuH&%$aJZ+D1< kz^8(E(RJLn[lkDD*%MSyrw],.,y)4uS([l`bD*:MIjZ');
define('NONCE_KEY',        '1^U9mSSub}mqedctM0j5*,;Agn+fFZ4sb>!XdMJ>[I?<Pr(92lS+@Lfxah}veX!G');
define('AUTH_SALT',        'GZcJI^E!5C5guima35qC7@]O~`2Es$@a02`h(|epT:B>k:fXg!#)0)/^B{7XK1vS');
define('SECURE_AUTH_SALT', '0*De$XB#|*fZc#mP*F0+~hM}BEx+p)kK7R9wqfSddLiJP>q9d[ZH<,FD6iL&Ubyt');
define('LOGGED_IN_SALT',   '9UM._cJcK.xh.aHDU=p2> p_CuY`Iy]|hd}[e~HaDM{6SNS-jum/7hZAYdW@Ym1O');
define('NONCE_SALT',       'Z*vQ,3Ro.o2,5nApZ+/^YVGRpjLb@66!-yAgmo8lI<~|`UGBY6w1Y*?k`/Y[xW4t');

/**#@-*/

/**
 * WordPress Database Table prefix.
 *
 * You can have multiple installations in one database if you give each
 * a unique prefix. Only numbers, letters, and underscores please!
 */
$table_prefix  = 'wp_';

/**
 * For developers: WordPress debugging mode.
 *
 * Change this to true to enable the display of notices during development.
 * It is strongly recommended that plugin and theme developers use WP_DEBUG
 * in their development environments.
 *
 * For information on other constants that can be used for debugging,
 * visit the Codex.
 *
 * @link https://codex.wordpress.org/Debugging_in_WordPress
 */
define('WP_DEBUG', false);

/* That's all, stop editing! Happy blogging. */

/** Absolute path to the WordPress directory. */
if ( !defined('ABSPATH') )
	define('ABSPATH', dirname(__FILE__) . '/');

/** Sets up WordPress vars and included files. */
require_once(ABSPATH . 'wp-settings.php');

