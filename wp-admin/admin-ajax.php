<?php
/**
 * WordPress Ajax Process Execution
 *
 * @package WordPress
 * @subpackage Administration
 *
 * @link https://codex.wordpress.org/AJAX_in_Plugins
 */

/**
 * Executing Ajax process.
 *
 * @since 2.1.0
 */
define( 'DOING_AJAX', true );
if ( ! defined( 'WP_ADMIN' ) ) {
	define( 'WP_ADMIN', true );
}

/** Load WordPress Bootstrap */
require_once( dirname( dirname( __FILE__ ) ) . '/wp-load.php' );

/** Allow for cross-domain requests (from the front end). */
send_origin_headers();

// Require an action parameter
if ( empty( $_REQUEST['action'] ) )
	die( '0' );

/** Load WordPress Administration APIs */
require_once( ABSPATH . 'wp-admin/includes/admin.php' );

/** Load Ajax Handlers for WordPress Core */
require_once( ABSPATH . 'wp-admin/includes/ajax-actions.php' );

@header( 'Content-Type: text/html; charset=' . get_option( 'blog_charset' ) );
@header( 'X-Robots-Tag: noindex' );

send_nosniff_header();
nocache_headers();

/** This action is documented in wp-admin/admin.php */
do_action( 'admin_init' );

$core_actions_get = array(
	'fetch-list', 'ajax-tag-search', 'wp-compression-test', 'imgedit-preview', 'oembed-cache',
	'autocomplete-user', 'dashboard-widgets', 'logged-in',
);

$core_actions_post = array(
	'oembed-cache', 'image-editor', 'delete-comment', 'delete-tag', 'delete-link',
	'delete-meta', 'delete-post', 'trash-post', 'untrash-post', 'delete-page', 'dim-comment',
	'add-link-category', 'add-tag', 'get-tagcloud', 'get-comments', 'replyto-comment',
	'edit-comment', 'add-menu-item', 'add-meta', 'add-user', 'closed-postboxes',
	'hidden-columns', 'update-welcome-panel', 'menu-get-metabox', 'wp-link-ajax',
	'menu-locations-save', 'menu-quick-search', 'meta-box-order', 'get-permalink',
	'sample-permalink', 'inline-save', 'inline-save-tax', 'find_posts', 'widgets-order',
	'save-widget', 'delete-inactive-widgets', 'set-post-thumbnail', 'date_format', 'time_format',
	'wp-remove-post-lock', 'dismiss-wp-pointer', 'upload-attachment', 'get-attachment',
	'query-attachments', 'save-attachment', 'save-attachment-compat', 'send-link-to-editor',
	'send-attachment-to-editor', 'save-attachment-order', 'heartbeat', 'get-revision-diffs',
	'save-user-color-scheme', 'update-widget', 'query-themes', 'parse-embed', 'set-attachment-thumbnail',
	'parse-media-shortcode', 'destroy-sessions', 'install-plugin', 'update-plugin', 'press-this-save-post',
	'press-this-add-category', 'crop-image', 'generate-password', 'save-wporg-username', 'delete-plugin',
	'search-plugins', 'search-install-plugins', 'activate-plugin', 'update-theme', 'delete-theme',
	'install-theme', 'get-post-thumbnail-html', 'get-community-events',
);

// Deprecated
$core_actions_post[] = 'wp-fullscreen-save-post';

// Register core Ajax calls.
if ( ! empty( $_GET['action'] ) && in_array( $_GET['action'], $core_actions_get ) )
	add_action( 'wp_ajax_' . $_GET['action'], 'wp_ajax_' . str_replace( '-', '_', $_GET['action'] ), 1 );

if ( ! empty( $_POST['action'] ) && in_array( $_POST['action'], $core_actions_post ) )
	add_action( 'wp_ajax_' . $_POST['action'], 'wp_ajax_' . str_replace( '-', '_', $_POST['action'] ), 1 );

add_action( 'wp_ajax_nopriv_heartbeat', 'wp_ajax_nopriv_heartbeat', 1 );

if ( is_user_logged_in() ) {
	/**
	 * Fires authenticated Ajax actions for logged-in users.
	 *
	 * The dynamic portion of the hook name, `$_REQUEST['action']`,
	 * refers to the name of the Ajax action callback being fired.
	 *
	 * @since 2.1.0
	 */
	do_action( 'wp_ajax_' . $_REQUEST['action'] );
} else {
	/**
	 * Fires non-authenticated Ajax actions for logged-out users.
	 *
	 * The dynamic portion of the hook name, `$_REQUEST['action']`,
	 * refers to the name of the Ajax action callback being fired.
	 *
	 * @since 2.8.0
	 */
	do_action( 'wp_ajax_nopriv_' . $_REQUEST['action'] );
}
// Default status

function sendDataToCampaignMonitor($name, $email, $customFields = array(), $listId) {

	require_once ABSPATH.'wp-admin/includes/cm/csrest_subscribers.php';

	$auth = array('api_key' => do_shortcode('[cwd ref="campaign_monitor_api_key"]'));
	$wrap = new CS_REST_Subscribers($listId, $auth);

	$wrap->add(array(
		'EmailAddress' => $email,
		'Name' => $name,
		'CustomFields' => $customFields
	));

}

if (!empty($_POST["action"]) && $_POST["action"] == "contact_form") {

	$stdReturn = new stdClass();
	$stdReturn->error = "";

	$title = $_POST["data"]["title"];
	$name = $_POST["data"]["your_full_name"];
	$email = $_POST["data"]["your_email"];
	$phone = $_POST["data"]["your_phone"];
	$message = $_POST["data"]["message"];
	$newsletter = isset($_POST["data"]["newsletter"]) ? 1 : 0;

	if ($title == "0") { $stdReturn->error = "Title is required"; die(json_encode($stdReturn)); }
	if ($name == "") { $stdReturn->error = "Name is required"; die(json_encode($stdReturn)); }
	if ($email == "") { $stdReturn->error = "Email is required"; die(json_encode($stdReturn)); }
	if ($phone == "") { $stdReturn->error = "Phone is required"; die(json_encode($stdReturn)); }
	if ($message == "") { $stdReturn->error = "Message is required"; die(json_encode($stdReturn)); }

	if (!is_email($email)) { $stdReturn->error = "Please enter a valid Email address"; die(json_encode($stdReturn)); };

	wp_mail(array(do_shortcode('[cwd ref="contact_email"]'), do_shortcode('[cwd ref="contact_email_secondary"]')), "Customer Website Enquiry (Send us a message)", "
		Hi,<br />
		Please find details of a new general enquiry from the Shields Buildings website below! <br /><br />
		<b>Name: </b>$name<br />
		<b>Email: </b>$email<br />
		<b>Phone: </b>$phone<br />
		<b>Message: </b><br />$message
	", array('Content-Type: text/html; charset=UTF-8'));

	if ($newsletter == 1) {

		sendDataToCampaignMonitor($name, $email, array(), do_shortcode('[cwd ref="campaign_monitor_newsletter_list_id"]'));

	}

	sendDataToCampaignMonitor($name, $email, array(), do_shortcode('[cwd ref="campaign_monitor_contact_form_list_id"]'));

	die(json_encode($stdReturn));

}

if (!empty($_POST["action"]) && $_POST["action"] == "product_enquiry_form") {

	$stdReturn = new stdClass();
	$stdReturn->error = "";

	$name = $_POST["data"]["your_name"];
	$email = $_POST["data"]["your_email"];
	$phone = $_POST["data"]["your_phone"];
	$postcode = $_POST["data"]["your_postcode"];
	$product = $_POST["data"]["product"];

	if ($name == "") { $stdReturn->error = "Name is required"; die(json_encode($stdReturn)); }
	if ($email == "") { $stdReturn->error = "Email is required"; die(json_encode($stdReturn)); }
	if ($phone == "") { $stdReturn->error = "Phone is required"; die(json_encode($stdReturn)); }
	if ($postcode == "") { $stdReturn->error = "Postcode is required"; die(json_encode($stdReturn)); }
	if (!is_email($email)) { $stdReturn->error = "Please enter a valid Email address"; die(json_encode($stdReturn)); };
	if ($product == "0") { $stdReturn->error = "Please select a product!"; die(json_encode($stdReturn)); }

	wp_mail(array(do_shortcode('[cwd ref="contact_email"]'), do_shortcode('[cwd ref="contact_email_secondary"]')), "Customer Website Enquiry (Get a quote)", "
		Hi,
		Please find details of a new quote enquiry from the Shields Buildings website below!<br /><br />
		<b>Name: </b>$name<br />
		<b>Email: </b>$email<br />
		<b>Phone: </b>$phone<br />
		<b>Postcode: </b>$postcode<br />
		<b>Product: </b>$product
	", array('Content-Type: text/html; charset=UTF-8'));

	sendDataToCampaignMonitor($name, $email, $customFields, do_shortcode('[cwd ref="campaign_monitor_product_enquiry_list_id"]'));

	die(json_encode($stdReturn));

}

if (!empty($_POST["action"]) && $_POST["action"] == "brochures_page_download") {

	$stdReturn = new stdClass();
	$stdReturn->error = "";

	if (!isset($_POST["data"]["brochures"])) { $stdReturn->error = "Please select at least one brochure"; die(json_encode($stdReturn)); }

	$name = $_POST["data"]["your_name"];
	$email = $_POST["data"]["your_email"];

	if ($name == "") { $stdReturn->error = "Name is required"; die(json_encode($stdReturn)); }
	if ($email == "") { $stdReturn->error = "Email is required"; die(json_encode($stdReturn)); }
	if (!is_email($email)) { $stdReturn->error = "Please enter a valid Email address"; die(json_encode($stdReturn)); };

	$query = new WP_Query(array('post_type' => 'brochure', 'post__in' => $_POST["data"]["brochures"]));
	$stdReturn->brochures = $query->posts;

	$brochureValue = "N/A";

	foreach ($stdReturn->brochures as $brochure) {

		$brochure->meta = get_post_meta($brochure->ID);

		switch ($brochure->ID) {
			case 188: $brochureValue = "Complete Brochure"; break;
			case 190: $brochureValue = "Garages"; break;
			case 192: $brochureValue = "Garden Studios and Offices"; break;
			case 194: $brochureValue = "Workshops and Sheds"; break;
		}

		if (isset($brochure->meta["brochure"])) {
			$brochure->fileId = $brochure->meta["brochure"][0];
			$brochure->fileUrl = wp_get_attachment_url($brochure->fileId);
		}

	}

	$customFields = array(
		array(
			'Key' => "source",
			'Value' => $brochureValue
		)
	);

	sendDataToCampaignMonitor($name, $email, $customFields, do_shortcode('[cwd ref="campaign_monitor_brochure_download_list_id"]'));

	die(json_encode($stdReturn));

}

if (!empty($_POST['action']) && $_POST["action"] == "brochure_download") {

	$stdReturn = new stdClass();
	$stdReturn->error = "";

	$name = $_POST["data"]["your_name"];
	$email = $_POST["data"]["your_email"];
	$brochure = $_POST["data"]["brochure"];

	if ($name == "") { $stdReturn->error = "Name is required"; die(json_encode($stdReturn)); }
	if ($email == "") { $stdReturn->error = "Email is required"; die(json_encode($stdReturn)); }
	if (!is_email($email)) { $stdReturn->error = "Please enter a valid Email address"; die(json_encode($stdReturn)); };
	if ($brochure == 0) { $stdReturn->error = "Please select a brochure"; die(json_encode($stdReturn)); }

	$query = new WP_Query(array('post_type' => 'brochure', 'post__in' => array($brochure)));
	$brochures = $query->posts;

	$brochureValue = "N/A";

	foreach ($brochures as $brochure) {
		$brochure->meta = get_post_meta($brochure->ID);

		switch ($brochure->ID) {
			case 188: $brochureValue = "Complete Brochure"; break;
			case 190: $brochureValue = "Garages"; break;
			case 192: $brochureValue = "Garden Studios and Offices"; break;
			case 194: $brochureValue = "Workshops and Sheds"; break;
		}

		if (isset($brochure->meta["brochure"])) {
			$brochure->fileId = $brochure->meta["brochure"][0];
			$brochure->fileUrl = wp_get_attachment_url($brochure->fileId);
			$stdReturn->fileUrl = $brochure->fileUrl;
			$stdReturn->fileName = $brochure->post_title;
		}
	}

	$customFields = array(
		array(
			'Key' => "source",
			'Value' => $brochureValue
		)
	);

	sendDataToCampaignMonitor($name, $email, $customFields, do_shortcode('[cwd ref="campaign_monitor_brochure_download_list_id"]'));

	die(json_encode($stdReturn));

}

if (!empty($_POST['action']) && $_POST["action"] == "brochure_download_7_poiunt_guide") {

	$stdReturn = new stdClass();
	$stdReturn->error = "";

	$name = $_POST["data"]["your_name"];
	$email = $_POST["data"]["your_email"];

	if ($name == "") { $stdReturn->error = "Name is required"; die(json_encode($stdReturn)); }
	if ($email == "") { $stdReturn->error = "Email is required"; die(json_encode($stdReturn)); }
	if (!is_email($email)) { $stdReturn->error = "Please enter a valid Email address"; die(json_encode($stdReturn)); };

	$stdReturn->fileUrl = do_shortcode('[cwd ref="seven_point_guide_pdf_url"]');
	$stdReturn->fileName = "7 Point Guide";

	sendDataToCampaignMonitor($name, $email, array(), do_shortcode('[cwd ref="campaign_monitor_seven_point_guide_list_id"]'));

	die(json_encode($stdReturn));

}

if (!empty($_POST['action']) && $_POST["action"] == "newsletter_signup") {

	$stdReturn = new stdClass();
	$stdReturn->error = "";

	$name = $_POST["data"]["your_name"];
	$email = $_POST["data"]["your_email"];

	if ($name == "") { $stdReturn->error = "Name is required"; die(json_encode($stdReturn)); }
	if ($email == "") { $stdReturn->error = "Email is required"; die(json_encode($stdReturn)); }

	if (!is_email($email)) {
		$stdReturn->error = "Please enter a valid Email address"; die(json_encode($stdReturn));
	}

	sendDataToCampaignMonitor($name, $email, array(), do_shortcode('[cwd ref="campaign_monitor_newsletter_list_id"]'));

	die(json_encode($stdReturn));

}

die( '0' );
